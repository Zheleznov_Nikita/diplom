<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <%--@elvariable id="bankAccountForm" type="model"--%>
    <form:form method="POST" modelAttribute="bankAccountForm">
        <div>
            <form:input type="number" id="myRange" path="sum" placeholder="Сумма"
                        autofocus="true" min="0" step="100"></form:input>
        </div>
        <button type="submit"> Открыть</button>
    </form:form>
</div>
<a href="/lk">Личный кабинет</a>
</body>
</html>