<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <sec:authorize access="hasRole('USER')">
        <table>
            <c:forEach items="${allCredits}" var="creditEntity">
                <tr>
                    <table>
                        <thead>
                        <th>Сумма</th>
                        <th>Дата начала</th>
                        <th>Дата конца</th>
                        <th>Пени</th>
                        <th>Статус</th>
                        </thead>
                        <tr>
                            <td>${creditEntity.sum}</td>
                            <td>${creditEntity.startDate}</td>
                            <td>${creditEntity.endDate}</td>
                            <td>${creditEntity.penalization}</td>
                            <td>${creditEntity.creditStatus}</td>
                        </tr>
                    </table>
                </tr>
                <tr><a href="/payment?id=${creditEntity.id}">Погасить</a></tr>
            </c:forEach>
        </table>
        <%--@elvariable id="creditForm" type="model"--%>
        <form:form method="POST" modelAttribute="creditForm">
            <div>
                <form:input type="hidden" id="id" path="creditCardId" value="${creditCard.id}"></form:input>
                <form:input type="range" id="myRange" path="sumCredit" placeholder="Лимит карты"
                            autofocus="true" min="0" max="${creditCard.sumLimit}" step="1000"></form:input>
                <p>Сумма: <span id="demo"></span></p>
            </div>
            <script>
                var slider = document.getElementById("myRange");
                var output = document.getElementById("demo");
                output.innerHTML = slider.value;

                slider.oninput = function () {
                    output.innerHTML = this.value;
                }
            </script>
            <button type="submit" <c:if test="${flag == true}"><c:out value="disabled='disabled'"/></c:if>> Оформить
            </button>
        </form:form>
    </sec:authorize>
    <c:if test="${flag == true}"><c:out value="Имеется просроченная карта!"/></c:if>
</div>
<a href="/lk">Личный кабинет</a>
</body>
</html>