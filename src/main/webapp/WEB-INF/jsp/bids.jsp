<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <table>
        <thead>
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Отчество</th>
        <th>Лимит</th>
        <th>Валюта</th>
        <th>Дата начала</th>
        <th>Статус</th>
        <th>Процентная ставка</th>
        <th>Дата конца</th>
        </thead>
        <c:forEach items="${allBids}" var="bidEntity">
            <tr>
                <td>${bidEntity.clientEntity.surname}</td>
                <td>${bidEntity.clientEntity.firstname}</td>
                <td>${bidEntity.clientEntity.patronymic}</td>
                <td>${bidEntity.sumLimit}</td>
                <td>${bidEntity.valutaEntity.name}</td>
                <td>${bidEntity.bidDate}</td>
                <td>${bidEntity.bidStatus}</td>
                <form action="${pageContext.request.contextPath}/bids" method="post">
                    <input type="hidden" name="bidId" value="${bidEntity.id}"/>
                    <input type="hidden" name="action" value="submit"/>
                    <td><input type="number" step="0.1" name="percentIn"/></td>
                    <td><input type="date" name="endDateIn"/></td>
                    <td>
                        <button type="submit">Одобрить</button>
                    </td>
                </form>
                <form action="${pageContext.request.contextPath}/bids" method="post">
                    <input type="hidden" name="bidId" value="${bidEntity.id}"/>
                    <input type="hidden" name="action" value="decline"/>
                    <td>
                        <button type="submit">Отклонить</button>
                    </td>
                </form>
            </tr>
        </c:forEach>
    </table>
</div>
<a href="/">Главная</a>
</body>
</html>