<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <%--@elvariable id="transferForm" type="model"--%>
    <form:form method="POST" modelAttribute="transferForm">
        <form:input type="number" id="myRange" path="sum" placeholder="Лимит карты"
                    autofocus="true" min="10" max="1000000" step="10"></form:input>
        <form:select type="select" id="selectSource" path="source">
            <c:forEach items="${requisitesModel}" var="requisite">
                <option>${requisite}</option>
            </c:forEach>
        </form:select>
        <form:select type="select" id="selectRecipient" path="recipient">
            <c:forEach items="${requisitesModel}" var="requisite">
                <option>${requisite}</option>
            </c:forEach>
        </form:select>
        <button type="submit"> Отправить</button>
    </form:form>
</div>
<a href="/lk">Личный кабинет</a>
</body>
</html>