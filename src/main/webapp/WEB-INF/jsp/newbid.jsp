<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <%--@elvariable id="creditCardForm" type="model"--%>
    <form:form method="POST" modelAttribute="creditCardForm">
        <div>
            <form:input type="range" id="myRange" path="sumLimit" placeholder="Лимит карты"
                        autofocus="true" min="40000" max="500000" step="1000"></form:input>
            <p>Лимит: <span id="demo"></span></p>
        </div>
        <script>
            var slider = document.getElementById("myRange");
            var output = document.getElementById("demo");
            output.innerHTML = slider.value;

            slider.oninput = function () {
                output.innerHTML = this.value;
            }
        </script>

        <form:select type="select" id="selectValuta" path="valuta">
            <c:forEach items="${valutaForm}" var="valutaEntity">
                <option>${valutaEntity.name}</option>
            </c:forEach>
        </form:select>
        <button type="submit" <c:if test="${flag == true}"><c:out value="disabled='disabled'"/></c:if>> Отправить
        </button>
    </form:form>
    <c:if test="${flag == true}"><c:out value="Имеется просроченная карта!"/></c:if>
</div>
<a href="/lk">Личный кабинет</a>
</body>
</html>