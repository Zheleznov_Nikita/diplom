<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <table>
        <thead>
        <th>Название</th>
        <th>Минимальная сумма</th>
        <th>Максимальная сумма</th>
        <th>Процентная ставка</th>
        <th>Срок (месяцев)</th>
        <th>Автопродляемость</th>
        </thead>
        <c:forEach items="${allTariffs}" var="tariffEntity">
            <tr>
                <td>${tariffEntity.name}</td>
                <td>${tariffEntity.minSum}</td>
                <td>${tariffEntity.maxSum}</td>
                <td>${tariffEntity.percent}</td>
                <td>${tariffEntity.term}</td>
                <form action="${pageContext.request.contextPath}/newdeposit" method="post">
                    <input type="hidden" name="id" value="${tariffEntity.id}"/>
                    <td>
                        <input type="checkbox" name="renewable"/>
                    </td>
                    <td>
                        <input type="number" min="${tariffEntity.minSum}" max="${tariffEntity.maxSum}" name="balance"/>
                    </td>
                    <td>
                        <button type="submit">Оформить</button>
                    </td>
                </form>
            </tr>
        </c:forEach>
    </table>
</div>
<a href="/lk">Личный кабинет</a>
</body>
</html>