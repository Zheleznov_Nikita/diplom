<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <table>
        <thead>
        <th>Название</th>
        <th>Минимальная сумма</th>
        <th>Максимальная сумма</th>
        <th>Процентная ставка</th>
        <th>Срок (месяцев)</th>
        <th>Пополнение</th>
        <th>Снятие средст</th>
        <th>Закрытие раньше срока</th>
        <th>Капитализируемый</th>
        </thead>
        <c:forEach items="${allTariffs}" var="tariffEntity">
            <tr>
                <form action="${pageContext.request.contextPath}/admin/tariffs" method="post">
                    <input type="hidden" name="id" value="${tariffEntity.id}"/>
                    <td><input type="text" name="name" value="${tariffEntity.name}"/></td>
                    <td><input type="number" step="100.0" name="minSum" value="${tariffEntity.minSum}"/></td>
                    <td><input type="number" step="100.0" name="maxSum" value="${tariffEntity.maxSum}"/></td>
                    <td><input type="number" step="0.1" name="percent" value="${tariffEntity.percent}"/></td>
                    <td><input type="number" step="1.0" name="term" value="${tariffEntity.term}"/></td>
                    <td><input type="checkbox" name="refill" ${tariffEntity.refill?"checked":""}/></td>
                    <td><input type="checkbox" name="take" ${tariffEntity.take?"checked":""}/></td>
                    <td><input type="checkbox" name="close" ${tariffEntity.close?"checked":""}/></td>
                    <td><input type="checkbox" name="capitalization" ${tariffEntity.capitalization?"checked":""}/></td>
                    <input type="hidden" name="action" value="edit"/>
                    <td>
                        <button type="submit">Редактировать</button>
                    </td>
                </form>
                <form action="${pageContext.request.contextPath}/admin/tariffs" method="post">
                    <input type="hidden" name="id" value="${tariffEntity.id}"/>
                    <input type="hidden" name="action" value="delete"/>
                    <td>
                        <button type="submit">Удалить</button>
                    </td>
                </form>
            </tr>
        </c:forEach>
    </table>
    <a href="/admin/newtariff">Новый тариф</a>
</div>
<a href="/">Главная</a>
</body>
</html>