<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <table>
        <thead>
        <th>Сумма перевода</th>
        <th>Дата перевода</th>
        <th>Отправитель</th>
        <th>Получатель</th>
        </thead>
        <c:forEach items="${transferFromAccount}" var="transferFromAccountEntity">
            <tr>
                <td>${transferFromAccountEntity.sum}</td>
                <td>${transferFromAccountEntity.transferDate}</td>
                <td>${transferFromAccountEntity.sourceEntity.accountNumber}</td>
                <td>${transferFromAccountEntity.recipientNumber}</td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin/transfers" method="post">
                        <input type="hidden" name="id" value="${transferFromAccountEntity.id}"/>
                        <input type="hidden" name="type" value="BANK_ACCOUNT"/>
                        <input type="hidden" name="action" value="submit"/>
                        <button type="submit">Одобрить</button>
                    </form>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin/transfers" method="post">
                        <input type="hidden" name="id" value="${transferFromAccountEntity.id}"/>
                        <input type="hidden" name="type" value="BANK_ACCOUNT"/>
                        <input type="hidden" name="action" value="decline"/>
                        <button type="submit">Заблокировать</button>
                    </form>
                </td>
            </tr>
        </c:forEach>

        <c:forEach items="${transferFromCard}" var="transferFromCardEntity">
            <tr>
                <td>${transferFromCardEntity.sum}</td>
                <td>${transferFromCardEntity.transferDate}</td>
                <td>${transferFromCardEntity.sourceEntity.cardNumber}</td>
                <td>${transferFromCardEntity.recipientNumber}</td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin/transfers" method="post">
                        <input type="hidden" name="id" value="${transferFromCardEntity.id}"/>
                        <input type="hidden" name="type" value="CARD"/>
                        <input type="hidden" name="action" value="submit"/>
                        <button type="submit">Одобрить</button>
                    </form>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin/transfers" method="post">
                        <input type="hidden" name="id" value="${transferFromCardEntity.id}"/>
                        <input type="hidden" name="type" value="CARD"/>
                        <input type="hidden" name="action" value="decline"/>
                        <button type="submit">Заблокировать</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<a href="/">Главная</a>
</body>
</html>