<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <sec:authorize access="hasRole('USER')">
        <table>
            <thead>
            <th>Баланс</th>
            <th>дата начала</th>
            <th>дата конца</th>
            <th>Процентная ставка</th>
            <th>Статус</th>
            <th>Автопродление</th>
            </thead>
            <tr>
                <td>${depositEntity.balance}</td>
                <td>${depositEntity.startDate}</td>
                <td>${depositEntity.endDate}</td>
                <td>${depositEntity.tariffEntity.percent}</td>
                <td>${depositEntity.status}</td>
                <td>${depositEntity.autoRenewable}</td>
            </tr>
        </table>
        <form action="${pageContext.request.contextPath}/deposit" method="post">
            <input type="hidden" name="id" value="${depositEntity.id}"/>
            <c:if test="${action == 'take'}">
                <input type="hidden" name="action" value="TAKE"/>
                <input type="range" name="sum" id="sum" min="0" max="${depositEntity.balance}"/>
                <p>Сумма: <span id="demo"></span></p>
                <script>
                    var slider = document.getElementById("sum");
                    var output = document.getElementById("demo");
                    output.innerHTML = slider.value;

                    slider.oninput = function () {
                        output.innerHTML = this.value;
                    }
                </script>
                <button type="submit">Снять</button>
            </c:if>
            <c:if test="${action == 'fill'}">
                <input type="hidden" name="action" value="FILL"/>
                <input type="number" name="sum" min="0"/>
                <button type="submit">Пополнить</button>
            </c:if>
        </form>
    </sec:authorize>
</div>
<a href="/lk">Личный кабинет</a>
</body>
</html>