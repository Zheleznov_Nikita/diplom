<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Log in with your account</title>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>

<body>
<div>
    <table>
        <thead>
        <th>Пени</th>
        <th>Комиссия при оплате кредита в иностранной валюте</th>
        <th>Комиссия при переводе</th>
        </thead>
        <tr>
            <form action="${pageContext.request.contextPath}/commission" method="post">
                <input type="hidden" name="id" value="${comission.id}"/>
                <td><input type="number" step="0.1" name="peni" value="${comission.peni}"/></td>
                <td><input type="number" step="0.1" name="commission" value="${comission.commission}"/></td>
                <td><input type="number" step="0.1" name="transferCommission" value="${comission.transferCommission}"/>
                </td>
                <td>
                    <button type="submit">Применить</button>
                </td>
            </form>
        </tr>
    </table>
    <div>
        <h4><a href="/admin">Личный кабинет</a></h4>
    </div>
    <a href="/">Главная</a>
</div>
</body>
</html>