<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <h3>${pageContext.request.userPrincipal.name}</h3>
    <sec:authorize access="isAuthenticated()">
        <h4><a href="/logout">Выйти</a></h4>
        <h4><a href="/clientbids">Заявки на кредитные карты</a></h4>
        <h4><a href="/transfer">Новый перевод</a></h4>
        <h4><a href="/transfers">История переводов</a></h4>
        <h4><a href="/newdeposit">Новый вклад</a></h4>
    </sec:authorize>
</div>
<div>
    <h3>Дебетовые карты</h3>
    <table>
        <c:forEach items="${debitCards}" var="debitCardEntity">
            <tr>
                <table>
                    <thead>
                    <th>Номер карты</th>
                    <th>Сумма</th>
                    </thead>
                    <tr>
                        <td>${debitCardEntity.cardNumber}</td>
                        <td>${debitCardEntity.sum}</td>
                    </tr>
                </table>
            </tr>
        </c:forEach>
        </tr>
    </table>
    <h4><a href="/newcard">Открыть дебетовую карту</a></h4>
</div>
<div>
    <h3>Счета</h3>
    <table>
        <c:forEach items="${bankAccounts}" var="bankAccountEntity">
            <tr>
                <table>
                    <thead>
                    <th>Номер счёта</th>
                    <th>Сумма</th>
                    </thead>
                    <tr>
                        <td>${bankAccountEntity.accountNumber}</td>
                        <td>${bankAccountEntity.sum}</td>
                    </tr>
                </table>
            </tr>
        </c:forEach>
        </tr>
    </table>
    <h4><a href="/newbankaccount">Открыть счёт</a></h4>
</div>
<div>
    <h3>Вклады</h3>
    <table>
        <c:forEach items="${deposits}" var="depositEntity">
            <tr>
                <table>
                    <thead>
                    <th>Баланс</th>
                    <th>дата начала</th>
                    <th>дата конца</th>
                    <th>Процентная ставка</th>
                    <th>Статус</th>
                    <th>Автопродление</th>
                    </thead>
                    <tr>
                        <td>${depositEntity.balance}</td>
                        <td>${depositEntity.startDate}</td>
                        <td>${depositEntity.endDate}</td>
                        <td>${depositEntity.tariffEntity.percent}</td>
                        <td>${depositEntity.status}</td>
                        <td>${depositEntity.autoRenewable}</td>
                    </tr>
                </table>
            </tr>
            <tr>
                <table>
                    <c:if test="${depositEntity.tariffEntity.refill == true}">
                        <td>
                            <form action="${pageContext.request.contextPath}/lk" method="post">
                                <input type="hidden" name="id" value="${depositEntity.id}"/>
                                <input type="hidden" name="action" value="fill"/>
                                <button type="submit">Пополнить</button>
                            </form>
                        </td>
                    </c:if>
                    <c:if test="${depositEntity.tariffEntity.take == true}">
                        <td>
                            <form action="${pageContext.request.contextPath}/lk" method="post">
                                <input type="hidden" name="id" value="${depositEntity.id}"/>
                                <input type="hidden" name="action" value="take"/>
                                <button type="submit">Снять</button>
                            </form>
                        </td>
                    </c:if>
                    <c:if test="${depositEntity.tariffEntity.close == true}">
                        <td>
                            <form action="${pageContext.request.contextPath}/lk" method="post">
                                <input type="hidden" name="id" value="${depositEntity.id}"/>
                                <input type="hidden" name="action" value="close"/>
                                <button type="submit">Закрыть</button>
                            </form>
                        </td>
                    </c:if>
                </table>
            </tr>
        </c:forEach>
        </tr>
    </table>
</div>
<div>
    <h3>Открытые кредитные карты</h3>
    <table>
        <c:forEach items="${creditCards}" var="creditCardEntity">
            <tr>
                <table>
                    <thead>
                    <th>Лимит</th>
                    <th>Валюта</th>
                    <th>Процентная ставка</th>
                    <th>Дата окончания действия карты</th>
                    <th>Статус карты</th>
                    </thead>
                    <tr>
                        <td>${creditCardEntity.sumLimit}</td>
                        <td>${creditCardEntity.valutaEntity.name}</td>
                        <td>${creditCardEntity.percent}</td>
                        <td>${creditCardEntity.endDate}</td>
                        <td>${creditCardEntity.cardStatus}</td>
                    </tr>
                </table>
            </tr>
            <tr><a href="/credits?id=${creditCardEntity.id}">Подробнее</a></tr>
        </c:forEach>
        </tr>
    </table>
    <h4><a href="/newbid">Подать заявку на кредитную карту</a></h4>
    <h3>Предложения по кредитным картам</h3>
    <table>
        <thead>
        <th>Лимит</th>
        <th>Валюта</th>
        <th>Процентная ставка</th>
        <th>Дата окончания действия карты</th>
        <th>Статус карты</th>
        </thead>
        <c:forEach items="${creditCardsInProcess}" var="creditCardEntity">
            <tr>
                <td>${creditCardEntity.sumLimit}</td>
                <td>${creditCardEntity.valutaEntity.name}</td>
                <td>${creditCardEntity.percent}</td>
                <td>${creditCardEntity.endDate}</td>
                <td>${creditCardEntity.cardStatus}</td>
                <td>
                    <form action="${pageContext.request.contextPath}/lk" method="post">
                        <input type="hidden" name="id" value="${creditCardEntity.id}"/>
                        <input type="hidden" name="action" value="submit"/>
                        <button type="submit">Принять</button>
                    </form>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/lk" method="post">
                        <input type="hidden" name="id" value="${creditCardEntity.id}"/>
                        <input type="hidden" name="action" value="decline"/>
                        <button type="submit">Отклонить</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<a href="/">Главная</a>
</body>
</html>