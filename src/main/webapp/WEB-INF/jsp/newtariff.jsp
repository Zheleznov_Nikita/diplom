<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <table>
        <thead>
        <th>Название</th>
        <th>Минимальная сумма</th>
        <th>Максимальная сумма</th>
        <th>Процентная ставка</th>
        <th>Срок (месяцев)</th>
        <th>Пополнение</th>
        <th>Снятие средст</th>
        <th>Закрытие раньше срока</th>
        <th>Капитализируемый</th>
        </thead>
        <tr>
            <form action="${pageContext.request.contextPath}/admin/newtariff" method="post">
                <td><input type="text" name="name"/></td>
                <td><input type="number" step="100.0" name="minSum"/></td>
                <td><input type="number" step="100.0" name="maxSum"/></td>
                <td><input type="number" step="0.1" name="percent"/></td>
                <td><input type="number" step="1.0" name="term"/></td>
                <td><input type="checkbox" name="refill"/></td>
                <td><input type="checkbox" name="take"/></td>
                <td><input type="checkbox" name="close"/></td>
                <td><input type="checkbox" name="capitalization"/></td>
                <td>
                    <button type="submit">Применить</button>
                </td>
            </form>
        </tr>
    </table>
</div>
<a href="/admin">Личный кабинет</a>
</body>
</html>