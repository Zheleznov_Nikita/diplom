<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Log in with your account</title>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>

<body>
<div>
    <div>
        <h4><a href="/bids">Заявки</a></h4>
    </div>
    <div>
        <h4><a href="/commission">Комиссии</a></h4>
    </div>
    <div>
        <h4><a href="/admin/transfers">Переводы</a></h4>
    </div>
    <div>
        <h4><a href="/admin/tariffs">Тарифы по вкладам</a></h4>
    </div>
    <a href="/">Главная</a>
</div>
</body>
</html>