<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Регистрация</title>
</head>

<body>
<div>
    <h2>Необходимо погасить: </h2>
    <h2>${paymentSum} рублей</h2>
</div>
<div>
    <%--@elvariable id="paymentForm" type="model"--%>
    <form:form method="POST" modelAttribute="paymentForm">
        <h2>Погашение кредита</h2>
        <div>
            <form:input type="number" step = "0.01" path="sum" placeholder="Сумма"
                        autofocus="true" min="0" max="${paymentSum}"></form:input>
            <form:errors path="sum"></form:errors>
                ${sumError}
            <form:input type="hidden" path="creditEntity.id" value="${creditForm.id}"></form:input>

        </div>
        <button type="submit">Погасить</button>
    </form:form>
    <a href="/lk">Личный кабинет</a>
</div>
</body>
</html>