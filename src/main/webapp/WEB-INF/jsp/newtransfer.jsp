<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <sec:authorize access="hasRole('USER')">
        <h4><a href="/transfer/inner">Перевод между своими счетами или картами</a></h4>
        <h4><a href="/transfer/bynumber">Перевод по номеру карты или счета</a></h4>
        <h4><a href="/lk">Личный кабинет</a></h4>
    </sec:authorize>
</div>
</body>
</html>