<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>
<div>
    <h3>Переводы клиентам банка</h3>
    <table>
        <thead>
        <th>Отправитель</th>
        <th>Получатель</th>
        <th>Сумма</th>
        <th>Дата</th>
        </thead>
        <c:forEach items="${transferModel1}" var="accountToAccountEntity">
            <tr>
                <td>${accountToAccountEntity.sourceEntity.accountNumber}</td>
                <td>${accountToAccountEntity.recipientEntity.accountNumber}</td>
                <td>${accountToAccountEntity.sum}</td>
                <td>${accountToAccountEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${transferModel2}" var="accountToCardEntity">
            <tr>
                <td>${accountToCardEntity.sourceEntity.accountNumber}</td>
                <td>${accountToCardEntity.recipientEntity.cardNumber}</td>
                <td>${accountToCardEntity.sum}</td>
                <td>${accountToCardEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${transferModel3}" var="cardToAccountEntity">
            <tr>
                <td>${cardToAccountEntity.sourceEntity.cardNumber}</td>
                <td>${cardToAccountEntity.recipientEntity.accountNumber}</td>
                <td>${cardToAccountEntity.sum}</td>
                <td>${cardToAccountEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${transferModel4}" var="cardToCardEntity">
            <tr>
                <td>${cardToCardEntity.sourceEntity.cardNumber}</td>
                <td>${cardToCardEntity.recipientEntity.cardNumber}</td>
                <td>${cardToCardEntity.sum}</td>
                <td>${cardToCardEntity.transferDate}</td>
            </tr>
        </c:forEach>
    </table>


    <h3>Переводы клиентам других банков</h3>
    <table>
        <thead>
        <th>Отправитель</th>
        <th>Получатель</th>
        <th>Сумма</th>
        <th>Дата</th>
        <th>Статус</th>
        </thead>
        <c:forEach items="${transferModel5}" var="transferFromAccountEntity">
            <tr>
                <td>${transferFromAccountEntity.sourceEntity.accountNumber}</td>
                <td>${transferFromAccountEntity.recipientNumber}</td>
                <td>${transferFromAccountEntity.sum}</td>
                <td>${transferFromAccountEntity.transferDate}</td>
                <td>${transferFromAccountEntity.status}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${transferModel6}" var="transferFromCardEntity">
            <tr>
                <td>${transferFromCardEntity.sourceEntity.cardNumber}</td>
                <td>${transferFromCardEntity.recipientNumber}</td>
                <td>${transferFromCardEntity.sum}</td>
                <td>${transferFromCardEntity.transferDate}</td>
                <td>${transferFromCardEntity.status}</td>
            </tr>
        </c:forEach>
    </table>


    <h3>Зачисления</h3>
    <table>
        <thead>
        <th>Отправитель</th>
        <th>Получатель</th>
        <th>Сумма</th>
        <th>Дата</th>
        </thead>
        <c:forEach items="${enrollmentModel1}" var="accountToAccountEntity">
            <tr>
                <td>${accountToAccountEntity.sourceEntity.accountNumber}</td>
                <td>${accountToAccountEntity.recipientEntity.accountNumber}</td>
                <td>${accountToAccountEntity.sum}</td>
                <td>${accountToAccountEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${enrollmentModel2}" var="accountToCardEntity">
            <tr>
                <td>${accountToCardEntity.sourceEntity.accountNumber}</td>
                <td>${accountToCardEntity.recipientEntity.cardNumber}</td>
                <td>${accountToCardEntity.sum}</td>
                <td>${accountToCardEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${enrollmentModel3}" var="cardToAccountEntity">
            <tr>
                <td>${cardToAccountEntity.sourceEntity.cardNumber}</td>
                <td>${cardToAccountEntity.recipientEntity.accountNumber}</td>
                <td>${cardToAccountEntity.sum}</td>
                <td>${cardToAccountEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${enrollmentModel4}" var="cardToCardEntity">
            <tr>
                <td>${cardToCardEntity.sourceEntity.cardNumber}</td>
                <td>${cardToCardEntity.recipientEntity.cardNumber}</td>
                <td>${cardToCardEntity.sum}</td>
                <td>${cardToCardEntity.transferDate}</td>
            </tr>
        </c:forEach>
    </table>

    <h3>Переводы между своими картами и счетами</h3>
    <table>
        <thead>
        <th>Отправитель</th>
        <th>Получатель</th>
        <th>Сумма</th>
        <th>Дата</th>
        </thead>
        <c:forEach items="${innerTransferModel1}" var="accountToAccountEntity">
            <tr>
                <td>${accountToAccountEntity.sourceEntity.accountNumber}</td>
                <td>${accountToAccountEntity.recipientEntity.accountNumber}</td>
                <td>${accountToAccountEntity.sum}</td>
                <td>${accountToAccountEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${innerTransferModel2}" var="accountToCardEntity">
            <tr>
                <td>${accountToCardEntity.sourceEntity.accountNumber}</td>
                <td>${accountToCardEntity.recipientEntity.cardNumber}</td>
                <td>${accountToCardEntity.sum}</td>
                <td>${accountToCardEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${innerTransferModel3}" var="cardToAccountEntity">
            <tr>
                <td>${cardToAccountEntity.sourceEntity.cardNumber}</td>
                <td>${cardToAccountEntity.recipientEntity.accountNumber}</td>
                <td>${cardToAccountEntity.sum}</td>
                <td>${cardToAccountEntity.transferDate}</td>
            </tr>
        </c:forEach>
        <c:forEach items="${innerTransferModel4}" var="cardToCardEntity">
            <tr>
                <td>${cardToCardEntity.sourceEntity.cardNumber}</td>
                <td>${cardToCardEntity.recipientEntity.cardNumber}</td>
                <td>${cardToCardEntity.sum}</td>
                <td>${cardToCardEntity.transferDate}</td>
            </tr>
        </c:forEach>
    </table>

</div>
<a href="/lk">Личный кабинет</a>
</body>
</html>