package com.diplom.entity.task3;

import com.diplom.entity.general.ClientEntity;
import com.diplom.enums.task3.DepositStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "deposit")
public class DepositEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date startDate;
    private Date endDate;
    private BigDecimal startBalance;
    private BigDecimal balance;
    @Enumerated(EnumType.STRING)
    private DepositStatus status;
    private boolean autoRenewable;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tariff_id")
    private TariffEntity tariffEntity;
    @ManyToOne(fetch = FetchType.LAZY)
    private ClientEntity clientEntity;
    @OneToMany(mappedBy = "depositEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<DepositOperationEntity> operationEntities;

    public BigDecimal getStartBalance() {
        return startBalance;
    }

    public void setStartBalance(BigDecimal startBalance) {
        this.startBalance = startBalance;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isAutoRenewable() {
        return autoRenewable;
    }

    public void setAutoRenewable(boolean autoRenewable) {
        this.autoRenewable = autoRenewable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public DepositStatus getStatus() {
        return status;
    }

    public void setStatus(DepositStatus status) {
        this.status = status;
    }

    public TariffEntity getTariffEntity() {
        return tariffEntity;
    }

    public void setTariffEntity(TariffEntity tariffEntity) {
        this.tariffEntity = tariffEntity;
    }

    public ClientEntity getClientEntity() {
        return clientEntity;
    }

    public void setClientEntity(ClientEntity clientEntity) {
        this.clientEntity = clientEntity;
    }

    public Set<DepositOperationEntity> getOperationEntities() {
        return operationEntities;
    }

    public void setOperationEntities(Set<DepositOperationEntity> operationEntities) {
        this.operationEntities = operationEntities;
    }
}
