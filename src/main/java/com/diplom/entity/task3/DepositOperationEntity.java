package com.diplom.entity.task3;

import com.diplom.enums.task3.OperationType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "deposit_operation")
public class DepositOperationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal sumOperation;
    private BigDecimal minDepositBalance;
    @Enumerated(EnumType.STRING)
    private OperationType operationType;
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    private DepositEntity depositEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSumOperation() {
        return sumOperation;
    }

    public void setSumOperation(BigDecimal sumOperation) {
        this.sumOperation = sumOperation;
    }

    public BigDecimal getMinDepositBalance() {
        return minDepositBalance;
    }

    public void setMinDepositBalance(BigDecimal depositBalance) {
        this.minDepositBalance = depositBalance;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DepositEntity getDepositEntity() {
        return depositEntity;
    }

    public void setDepositEntity(DepositEntity depositEntity) {
        this.depositEntity = depositEntity;
    }
}
