package com.diplom.entity.general;


import javax.persistence.*;

@Entity
@Table(name = "percents")
public class CommissionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private double peni;
    private double commission;
    private double transferCommission;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPeni() {
        return peni;
    }

    public void setPeni(double percent) {
        this.peni = percent;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public double getTransferCommission() {
        return transferCommission;
    }

    public void setTransferCommission(double transferCommission) {
        this.transferCommission = transferCommission;
    }
}
