package com.diplom.entity.general;

import com.diplom.entity.task1.BidEntity;
import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.entity.task2.BankAccountEntity;
import com.diplom.entity.task2.DebitCardEntity;
import com.diplom.entity.task3.DepositEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "client")
public class ClientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Size(min = 2, message = "Не меньше 2 знаков")
    private String firstname;
    @Size(min = 2, message = "Не меньше 2 знаков")
    private String surname;
    @Size(min = 2, message = "Не меньше 2 знаков")
    private String patronymic;
    private String phone;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    //task1
    @OneToMany(mappedBy = "clientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CreditCardEntity> creditCardEntities;
    @OneToMany(mappedBy = "clientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<BidEntity> bidEntities;

    //task2
    @OneToMany(mappedBy = "clientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<BankAccountEntity> bankAccountEntities;
    @OneToMany(mappedBy = "clientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<DebitCardEntity> debitCardEntities;

    //task3
    @OneToMany(mappedBy = "clientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<DepositEntity> depositEntities;

    public ClientEntity() {

    }

    public ClientEntity(@Size(min = 2, message = "Не меньше 2 знаков") String firstname,
                        @Size(min = 2, message = "Не меньше 2 знаков") String surname,
                        @Size(min = 2, message = "Не меньше 2 знаков") String patronymic,
                        String phone) {
        this.firstname = firstname;
        this.surname = surname;
        this.patronymic = patronymic;
        this.phone = phone;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Set<CreditCardEntity> getCreditCardEntities() {
        return creditCardEntities;
    }

    public void setCreditCardEntities(Set<CreditCardEntity> creditCardEntities) {
        this.creditCardEntities = creditCardEntities;
    }

    public Set<BidEntity> getBidEntities() {
        return bidEntities;
    }

    public void setBidEntities(Set<BidEntity> bidEntities) {
        this.bidEntities = bidEntities;
    }

    public Set<BankAccountEntity> getBankAccountEntities() {
        return bankAccountEntities;
    }

    public void setBankAccountEntities(Set<BankAccountEntity> bankAccountEntities) {
        this.bankAccountEntities = bankAccountEntities;
    }

    public Set<DebitCardEntity> getDebitCardEntities() {
        return debitCardEntities;
    }

    public void setDebitCardEntities(Set<DebitCardEntity> debitCardEntities) {
        this.debitCardEntities = debitCardEntities;
    }
}
