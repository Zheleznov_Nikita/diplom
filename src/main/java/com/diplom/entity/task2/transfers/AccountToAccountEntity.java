package com.diplom.entity.task2.transfers;

import com.diplom.entity.task2.BankAccountEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "transfer_account_to_account")
public class AccountToAccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal sum;
    private Date transferDate;
    @ManyToOne(fetch = FetchType.LAZY)
    private BankAccountEntity sourceEntity;
    @ManyToOne(fetch = FetchType.LAZY)
    private BankAccountEntity recipientEntity;

    public AccountToAccountEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public BankAccountEntity getSourceEntity() {
        return sourceEntity;
    }

    public void setSourceEntity(BankAccountEntity sourceEntity) {
        this.sourceEntity = sourceEntity;
    }

    public BankAccountEntity getRecipientEntity() {
        return recipientEntity;
    }

    public void setRecipientEntity(BankAccountEntity recipientEntity) {
        this.recipientEntity = recipientEntity;
    }
}
