package com.diplom.entity.task2.transfers;

import com.diplom.entity.task2.BankAccountEntity;
import com.diplom.enums.task2.TransferStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "transfer_out_from_account")
public class TransferFromAccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal sum;
    private Date transferDate;
    @ManyToOne(fetch = FetchType.LAZY)
    private BankAccountEntity sourceEntity;
    private String recipientNumber;
    @Enumerated(EnumType.STRING)
    private TransferStatus status;

    public TransferFromAccountEntity() {
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(TransferStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public BankAccountEntity getSourceEntity() {
        return sourceEntity;
    }

    public void setSourceEntity(BankAccountEntity sourceEntity) {
        this.sourceEntity = sourceEntity;
    }

    public String getRecipientNumber() {
        return recipientNumber;
    }

    public void setRecipientNumber(String recipientNumber) {
        this.recipientNumber = recipientNumber;
    }
}
