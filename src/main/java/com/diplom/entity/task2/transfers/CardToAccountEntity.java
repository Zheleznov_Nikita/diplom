package com.diplom.entity.task2.transfers;

import com.diplom.entity.task2.BankAccountEntity;
import com.diplom.entity.task2.DebitCardEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "transfer_card_to_account")
public class CardToAccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal sum;
    private Date transferDate;
    @ManyToOne(fetch = FetchType.LAZY)
    private DebitCardEntity sourceEntity;
    @ManyToOne(fetch = FetchType.LAZY)
    private BankAccountEntity recipientEntity;

    public CardToAccountEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public DebitCardEntity getSourceEntity() {
        return sourceEntity;
    }

    public void setSourceEntity(DebitCardEntity sourceEntity) {
        this.sourceEntity = sourceEntity;
    }

    public BankAccountEntity getRecipientEntity() {
        return recipientEntity;
    }

    public void setRecipientEntity(BankAccountEntity recipientEntity) {
        this.recipientEntity = recipientEntity;
    }
}
