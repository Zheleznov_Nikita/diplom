package com.diplom.entity.task2;

import com.diplom.entity.general.ClientEntity;
import com.diplom.entity.task2.transfers.AccountToCardEntity;
import com.diplom.entity.task2.transfers.CardToAccountEntity;
import com.diplom.entity.task2.transfers.CardToCardEntity;
import com.diplom.entity.task2.transfers.TransferFromCardEntity;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "debit_card")
public class DebitCardEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String cardNumber;
    @DecimalMin("0.0")
    private BigDecimal sum;
    @ManyToOne(fetch = FetchType.LAZY)
    private ClientEntity clientEntity;
    @OneToMany(mappedBy = "sourceEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CardToAccountEntity> cardToAccountEntities;
    @OneToMany(mappedBy = "sourceEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CardToCardEntity> cardToCardEntitiesSource;
    @OneToMany(mappedBy = "recipientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CardToCardEntity> cardToCardEntitiesRecipient;
    @OneToMany(mappedBy = "recipientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<AccountToCardEntity> accountToCardEntities;
    @OneToMany(mappedBy = "sourceEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<TransferFromCardEntity> transferFromCardEntities;


    public DebitCardEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public ClientEntity getClientEntity() {
        return clientEntity;
    }

    public void setClientEntity(ClientEntity clientEntity) {
        this.clientEntity = clientEntity;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Set<CardToAccountEntity> getCardToAccountEntities() {
        return cardToAccountEntities;
    }

    public void setCardToAccountEntities(Set<CardToAccountEntity> cardToAccountEntities) {
        this.cardToAccountEntities = cardToAccountEntities;
    }

    public Set<CardToCardEntity> getCardToCardEntitiesSource() {
        return cardToCardEntitiesSource;
    }

    public void setCardToCardEntitiesSource(Set<CardToCardEntity> cardToCardEntitiesSource) {
        this.cardToCardEntitiesSource = cardToCardEntitiesSource;
    }

    public Set<CardToCardEntity> getCardToCardEntitiesRecipient() {
        return cardToCardEntitiesRecipient;
    }

    public void setCardToCardEntitiesRecipient(Set<CardToCardEntity> cardToCardEntitiesRecipient) {
        this.cardToCardEntitiesRecipient = cardToCardEntitiesRecipient;
    }

    public Set<AccountToCardEntity> getAccountToCardEntities() {
        return accountToCardEntities;
    }

    public void setAccountToCardEntities(Set<AccountToCardEntity> accountToCardEntities) {
        this.accountToCardEntities = accountToCardEntities;
    }

    public Set<TransferFromCardEntity> getTransferFromCardEntities() {
        return transferFromCardEntities;
    }

    public void setTransferFromCardEntities(Set<TransferFromCardEntity> transferFromCardEntities) {
        this.transferFromCardEntities = transferFromCardEntities;
    }
}
