package com.diplom.entity.task2;


import com.diplom.entity.general.ClientEntity;
import com.diplom.entity.task2.transfers.AccountToAccountEntity;
import com.diplom.entity.task2.transfers.AccountToCardEntity;
import com.diplom.entity.task2.transfers.CardToAccountEntity;
import com.diplom.entity.task2.transfers.TransferFromAccountEntity;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "bank_account")
public class BankAccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String accountNumber;
    @DecimalMin("0.0")
    private BigDecimal sum;
    @ManyToOne(fetch = FetchType.LAZY)
    private ClientEntity clientEntity;
    @OneToMany(mappedBy = "sourceEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<AccountToAccountEntity> accountToAccountEntitiesSource;
    @OneToMany(mappedBy = "recipientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<AccountToAccountEntity> accountToAccountEntitiesRecipient;
    @OneToMany(mappedBy = "sourceEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<AccountToCardEntity> accountToCardEntities;
    @OneToMany(mappedBy = "recipientEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CardToAccountEntity> cardToAccountEntities;
    @OneToMany(mappedBy = "sourceEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<TransferFromAccountEntity> transferFromAccountEntities;

    public BankAccountEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public ClientEntity getClientEntity() {
        return clientEntity;
    }

    public void setClientEntity(ClientEntity clientEntity) {
        this.clientEntity = clientEntity;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Set<AccountToAccountEntity> getAccountToAccountEntitiesSource() {
        return accountToAccountEntitiesSource;
    }

    public void setAccountToAccountEntitiesSource(Set<AccountToAccountEntity> accountToAccountEntitiesSource) {
        this.accountToAccountEntitiesSource = accountToAccountEntitiesSource;
    }

    public Set<AccountToAccountEntity> getAccountToAccountEntitiesRecipient() {
        return accountToAccountEntitiesRecipient;
    }

    public void setAccountToAccountEntitiesRecipient(Set<AccountToAccountEntity> accountToAccountEntitiesRecipient) {
        this.accountToAccountEntitiesRecipient = accountToAccountEntitiesRecipient;
    }

    public Set<AccountToCardEntity> getAccountToCardEntities() {
        return accountToCardEntities;
    }

    public void setAccountToCardEntities(Set<AccountToCardEntity> accountToCardEntities) {
        this.accountToCardEntities = accountToCardEntities;
    }

    public Set<CardToAccountEntity> getCardToAccountEntities() {
        return cardToAccountEntities;
    }

    public void setCardToAccountEntities(Set<CardToAccountEntity> cardToAccountEntities) {
        this.cardToAccountEntities = cardToAccountEntities;
    }

    public Set<TransferFromAccountEntity> getTransferFromAccountEntities() {
        return transferFromAccountEntities;
    }

    public void setTransferFromAccountEntities(Set<TransferFromAccountEntity> transferFromAccountEntities) {
        this.transferFromAccountEntities = transferFromAccountEntities;
    }
}