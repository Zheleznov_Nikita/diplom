package com.diplom.entity.task1;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "payment")
public class PaymentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @DecimalMin(value = "0.0", message = "не менее 0")
    private BigDecimal sum;
    private Date paymentDate;
    @ManyToOne(fetch = FetchType.LAZY)
    private CreditEntity creditEntity;

    public PaymentEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public CreditEntity getCreditEntity() {
        return creditEntity;
    }

    public void setCreditEntity(CreditEntity creditEntity) {
        this.creditEntity = creditEntity;
    }
}
