package com.diplom.entity.task1;

import com.diplom.entity.general.ClientEntity;
import com.diplom.enums.task1.CardStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "credit_card")
public class CreditCardEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal sumLimit;
    private double percent;
    private Date endDate;
    @Enumerated(EnumType.STRING)
    private CardStatus cardStatus;
    @ManyToOne(fetch = FetchType.LAZY)
    private ClientEntity clientEntity;
    @OneToMany(mappedBy = "creditCardEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CreditEntity> creditEntities;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "valuta_id")
    private ValutaEntity valutaEntity;

    public CreditCardEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSumLimit() {
        return sumLimit;
    }

    public void setSumLimit(BigDecimal sumLimit) {
        this.sumLimit = sumLimit;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }

    public Set<CreditEntity> getCreditEntities() {
        return creditEntities;
    }

    public void setCreditEntities(Set<CreditEntity> creditEntities) {
        this.creditEntities = creditEntities;
    }

    public ClientEntity getClientEntity() {
        return clientEntity;
    }

    public void setClientEntity(ClientEntity clientEntity) {
        this.clientEntity = clientEntity;
    }

    public ValutaEntity getValutaEntity() {
        return valutaEntity;
    }

    public void setValutaEntity(ValutaEntity valutaEntity) {
        this.valutaEntity = valutaEntity;
    }
}
