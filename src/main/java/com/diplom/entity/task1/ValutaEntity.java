package com.diplom.entity.task1;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "valuta")
public class ValutaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
    private BigDecimal rubRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getRubRate() {
        return rubRate;
    }

    public void setRubRate(BigDecimal rubRate) {
        this.rubRate = rubRate;
    }
}