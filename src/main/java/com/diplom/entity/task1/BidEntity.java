package com.diplom.entity.task1;

import com.diplom.entity.general.ClientEntity;
import com.diplom.enums.task1.BidStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "bid")
public class BidEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal sumLimit;
    private Date bidDate;
    @Enumerated(EnumType.STRING)
    private BidStatus bidStatus;
    @ManyToOne(fetch = FetchType.LAZY)
    private ClientEntity clientEntity;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "valuta_id")
    private ValutaEntity valutaEntity;

    public ValutaEntity getValutaEntity() {
        return valutaEntity;
    }

    public void setValutaEntity(ValutaEntity valutaEntity) {
        this.valutaEntity = valutaEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSumLimit() {
        return sumLimit;
    }

    public void setSumLimit(BigDecimal sumLimit) {
        this.sumLimit = sumLimit;
    }

    public ClientEntity getClientEntity() {
        return clientEntity;
    }

    public void setClientEntity(ClientEntity clientEntity) {
        this.clientEntity = clientEntity;
    }

    public BidStatus getBidStatus() {
        return bidStatus;
    }

    public void setBidStatus(BidStatus bidStatus) {
        this.bidStatus = bidStatus;
    }

    public Date getBidDate() {
        return bidDate;
    }

    public void setBidDate(Date bidDate) {
        this.bidDate = bidDate;
    }
}
