package com.diplom.scheduler;

import com.diplom.entity.task3.DepositEntity;
import com.diplom.entity.task3.DepositOperationEntity;
import com.diplom.enums.task3.DepositStatus;
import com.diplom.enums.task3.OperationType;
import com.diplom.service.task3.DepositOperationService;
import com.diplom.service.task3.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class DepositScheduler {
    @Autowired
    private DepositService depositService;
    @Autowired
    private DepositOperationService depositOperationService;

    @Scheduled(cron = "0 40 19 * * ?")
    public void cronJobSch() {
        Calendar calendar = Calendar.getInstance();
        Date todayDate = Calendar.getInstance().getTime();
        calendar.setTime(todayDate);
        calendar.add(Calendar.DATE, 30);
        Date endDate = calendar.getTime();
        List<DepositEntity> depositEntities = depositService.findDepositEntitiesByAutoRenewable(true);
        for (DepositEntity depositEntity : depositEntities) {
            depositEntity.setEndDate(new java.sql.Date(endDate.getTime()));
            depositService.save(depositEntity);
        }

        calendar = Calendar.getInstance();
        calendar.setTime(todayDate);
        depositEntities = depositService.findDepositEntitiesByStatus(DepositStatus.OPEN, calendar.get(Calendar.DAY_OF_MONTH), new java.sql.Date(todayDate.getTime()));
        calendar.add(Calendar.MONTH, -1);
        Date monthAgo = calendar.getTime();
        for (DepositEntity depositEntity : depositEntities) {
            BigDecimal balance;
            if (depositEntity.getTariffEntity().isRefill() || depositEntity.getTariffEntity().isTake()) {
                DepositOperationEntity operation;
                if (depositOperationService.countAllByDateBetween(new java.sql.Date(todayDate.getTime()), new java.sql.Date(monthAgo.getTime())) > 0) {
                    operation = depositOperationService.findMinByDate(new java.sql.Date(todayDate.getTime()), new java.sql.Date(monthAgo.getTime()), depositEntity);
                } else {
                    operation = new DepositOperationEntity();
                    if (depositEntity.getTariffEntity().isCapitalization())
                        operation.setMinDepositBalance(depositEntity.getBalance());
                    else
                        operation.setMinDepositBalance(depositEntity.getStartBalance());
                }
                if (depositEntity.getTariffEntity().isCapitalization()) {
                    balance = depositEntity.getBalance()
                            .add(operation.getMinDepositBalance()
                                    .multiply(BigDecimal.valueOf(depositEntity.getTariffEntity().getPercent()))
                                    .multiply(BigDecimal.valueOf(0.01)))
                            .add(depositOperationService.findSumByType(OperationType.FROM_BANK, depositEntity));
                } else {
                    balance = depositEntity.getBalance()
                            .add(operation.getMinDepositBalance()
                                    .multiply(BigDecimal.valueOf(depositEntity.getTariffEntity().getPercent()))
                                    .multiply(BigDecimal.valueOf(0.01)));
                }
                depositEntity.setBalance(balance);
                depositService.save(depositEntity);

            } else {
                DepositOperationEntity depositOperationEntity = new DepositOperationEntity();
                depositOperationEntity.setDate(new java.sql.Date(todayDate.getTime()));
                depositOperationEntity.setOperationType(OperationType.FROM_BANK);
                depositOperationEntity.setMinDepositBalance(depositEntity.getBalance());
                if (depositEntity.getTariffEntity().isCapitalization()) {
                    depositEntity.setBalance(depositEntity.getBalance()
                            .add(depositEntity.getStartBalance()
                                    .multiply(BigDecimal.valueOf(depositEntity.getTariffEntity().getPercent()))
                                    .multiply(BigDecimal.valueOf(0.01)))
                            .add(depositOperationService.findSumByType(OperationType.FROM_BANK, depositEntity)));
                    depositOperationEntity.setSumOperation(depositEntity.getStartBalance()
                            .multiply(BigDecimal.valueOf(depositEntity.getTariffEntity().getPercent()))
                            .multiply(BigDecimal.valueOf(0.01))
                            .add(depositOperationService.findSumByType(OperationType.FROM_BANK, depositEntity)));
                } else {
                    depositEntity.setBalance(depositEntity.getBalance()
                            .add(depositEntity.getStartBalance()
                                    .multiply(BigDecimal.valueOf(depositEntity.getTariffEntity().getPercent()))
                                    .multiply(BigDecimal.valueOf(0.01))));
                    depositOperationEntity.setSumOperation(depositEntity.getStartBalance()
                            .multiply(BigDecimal.valueOf(depositEntity.getTariffEntity().getPercent()))
                            .multiply(BigDecimal.valueOf(0.01)));
                }
                depositOperationEntity.setDepositEntity(depositEntity);
                depositService.save(depositEntity);
                depositOperationService.save(depositOperationEntity);
            }
        }
    }
}
