package com.diplom.scheduler;

import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.entity.task1.CreditEntity;
import com.diplom.enums.task1.CardStatus;
import com.diplom.enums.task1.CreditStatus;
import com.diplom.service.task1.CommissionService;
import com.diplom.service.task1.CreditCardService;
import com.diplom.service.task1.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@Component
public class Scheduler {
    @Autowired
    private CreditCardService creditCardService;
    @Autowired
    private CreditService creditService;
    @Autowired
    private CommissionService commissionService;

    @Scheduled(cron = "50 30 12 * * ?")
    public void cronJobSch() {
        Date todayDate = new Date(Calendar.getInstance().getTime().getTime());
        List<CreditEntity> creditEntityList = creditService.findAllByTerm(CreditStatus.OPENED);
        for (CreditEntity creditEntity : creditEntityList) {
            if (todayDate.compareTo(creditEntity.getEndDate()) > 0) {
                creditEntity.setCreditStatus(CreditStatus.EXPIRED);
                creditService.save(creditEntity);
            }
        }
        creditEntityList = creditService.findAllByTerm(CreditStatus.EXPIRED);
        double peni = commissionService.findById(1L).getPeni();
        double creditPeni;
        for (CreditEntity creditEntity : creditEntityList) {
            creditPeni = creditEntity.getPenalization();
            if (creditPeni < 40) {
                creditPeni += peni;
                if (creditPeni <= 40)
                    creditEntity.setPenalization(creditPeni);
                else
                    creditEntity.setPenalization(40);
                creditService.save(creditEntity);
            }
        }
        List<CreditCardEntity> creditCardEntityList = creditCardService.findAll();
        for (CreditCardEntity creditCardEntity : creditCardEntityList) {
            if (creditCardEntity.getEndDate().compareTo(new Date(Calendar.getInstance().getTime().getTime())) <= 0) {
                if (creditService.findCountByCreditCardIdAndStatus(creditCardEntity.getId(), CreditStatus.REPAID) == 0)
                    creditCardEntity.setCardStatus(CardStatus.CLOSED);
                else
                    creditCardEntity.setCardStatus(CardStatus.EXPIRED);
                creditCardService.save(creditCardEntity);
            }
        }
    }
}
