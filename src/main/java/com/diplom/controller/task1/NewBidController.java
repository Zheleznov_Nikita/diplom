package com.diplom.controller.task1;

import com.diplom.dto.task1.BidDto;
import com.diplom.service.general.ClientService;
import com.diplom.service.task1.BidService;
import com.diplom.service.task1.CreditService;
import com.diplom.service.task1.ValutaService;
import com.diplom.validator.task1.NewCreditValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class NewBidController {
    private final ClientService clientService;
    private final BidService bidService;
    private final ValutaService valutaService;
    private final NewCreditValidator newCreditValidator;
    private final CreditService creditService;

    @Autowired
    public NewBidController(ClientService clientService,
                            BidService bidService,
                            NewCreditValidator newCreditValidator,
                            ValutaService valutaService,
                            CreditService creditService) {
        this.clientService = clientService;
        this.bidService = bidService;
        this.newCreditValidator = newCreditValidator;
        this.valutaService = valutaService;
        this.creditService = creditService;
    }

    @GetMapping("/newbid")
    public String newBid(Model model) {
        model.addAttribute("valutaForm", valutaService.getAll());
        model.addAttribute("creditCardForm", new BidDto());
        boolean flag = false;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        if (creditService.findCountByExpiredTerm(username) > 0)
            flag = true;
        model.addAttribute("flag", flag);
        return "newbid";
    }

    @PostMapping("/newbid")
    public String addBid(@ModelAttribute("creditCardForm") BidDto bidDto,
                         Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        if (creditService.findCountByExpiredTerm(username) > 0) {
            return "redirect:/clientbids";
        }
        bidService.newBid(bidDto, username);
        return "redirect:/clientbids";
    }
}