package com.diplom.controller.task1;

import com.diplom.service.task1.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;

@Controller
public class BidController {
    @Autowired
    private BidService bidService;

    @GetMapping("/bids")
    public String userList(Model model) {
        model.addAttribute("allBids", bidService.findInProgress());
        return "bids";
    }

    @PostMapping("/bids")
    public String checkBid(@RequestParam(required = true, defaultValue = "") Long bidId,
                           @RequestParam(required = true, defaultValue = "") String action,
                           @RequestParam(required = true, defaultValue = "") String percentIn,
                           @RequestParam(required = true, defaultValue = "") String endDateIn,
                           Model model) {
        if (action.equals("submit")) {
            bidService.submit(bidId, Double.parseDouble(percentIn), Date.valueOf(endDateIn));
        }
        if (action.equals("decline")) {
            bidService.decline(bidId);
        }
        return "redirect:/bids";
    }
}