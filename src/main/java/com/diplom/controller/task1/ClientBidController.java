package com.diplom.controller.task1;

import com.diplom.service.task1.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ClientBidController {
    @Autowired
    private BidService bidService;

    @GetMapping("/clientbids")
    public String userList(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        model.addAttribute("allBids", bidService.findByUsername(username));
        return "clientbids";
    }
}