package com.diplom.controller.task1;

import com.diplom.dto.task1.CreditDto;
import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.service.task1.CreditService;
import com.diplom.validator.task1.NewCreditValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class CreditsController {

    @Autowired
    private CreditService creditService;
    @Autowired
    private NewCreditValidator newCreditValidator;

    @GetMapping("/credits")
    public String userList(@RequestParam String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        CreditCardEntity creditCardEntity = newCreditValidator.getCreditCardEntity(Long.valueOf(id), username);
        model.addAttribute("creditCard", creditCardEntity);
        model.addAttribute("allCredits", creditService.findByCreditCardId(creditCardEntity));
        model.addAttribute("creditForm", new CreditDto());
        boolean flag = false;
        if (creditService.findCountByExpiredTerm(username) > 0)
            flag = true;
        model.addAttribute("flag", flag);
        return "credits";
    }

    @PostMapping("/credits")
    public String addCredit(@ModelAttribute("creditForm") @Valid CreditDto creditDto,
                            BindingResult bindingResult,
                            Model model) {
        if (bindingResult.hasErrors()) {
            return "redirect:/credits?id=" + creditDto.getCreditCardId().toString();
        }
        //кредит нельзя взять если есть просроченная карта
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        if (creditService.findCountByExpiredTerm(username) > 0) {
            return "redirect:/credits?id=" + creditDto.getCreditCardId().toString();
        }
        creditService.newCredit(creditDto, username);
        return "redirect:/credits?id=" + creditDto.getCreditCardId().toString();
    }
}
