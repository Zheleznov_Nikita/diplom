package com.diplom.controller.task1;

import com.diplom.dto.task1.PaymentDto;
import com.diplom.entity.task1.CreditEntity;
import com.diplom.service.task1.CreditService;
import com.diplom.service.task1.PaymentService;
import com.diplom.validator.task1.SumPaymentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.math.BigDecimal;

@Controller
public class PaymentController {
    @Autowired
    private SumPaymentValidator sumPaymentValidator;
    @Autowired
    private CreditService creditService;
    @Autowired
    private PaymentService paymentService;

    @GetMapping("/payment")
    public String registration(@RequestParam String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        BigDecimal paymentSum = sumPaymentValidator.getResultSum(Long.valueOf(id), username);
        model.addAttribute("paymentForm", new PaymentDto());
        model.addAttribute("paymentSum", paymentSum);
        CreditEntity creditEntity = creditService.findCreditEntityByIdAndUsername(Long.valueOf(id), username);
        model.addAttribute("creditForm", creditEntity);
        return "payment";
    }


    @PostMapping("/payment")
    public String addUser(@ModelAttribute("paymentForm") @Valid PaymentDto paymentDto,
                          BindingResult bindingResult,
                          Model model) {
        if (bindingResult.hasErrors()) {
            return "payment";
        }
        paymentService.newPayment(paymentDto);
        return "redirect:/lk";
    }
}
