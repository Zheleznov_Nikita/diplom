package com.diplom.controller.general;

import com.diplom.dto.task1.CommissionDto;
import com.diplom.service.task1.CommissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EditCommissionController {
    @Autowired
    private CommissionService commissionService;


    @GetMapping("/commission")
    public String userList(Model model) {
        model.addAttribute("comission", commissionService.findById(1L));
        model.addAttribute("commissionDto", new CommissionDto());
        return "editcommission";
    }

    @PostMapping("/commission")
    public String checkBid(@ModelAttribute("commissionDto") CommissionDto commissionDto,
                           Model model) {
        commissionService.save(commissionDto);
        return "redirect:/commission";
    }

}
