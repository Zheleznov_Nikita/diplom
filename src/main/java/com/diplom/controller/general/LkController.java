package com.diplom.controller.general;

import com.diplom.service.task1.CreditCardService;
import com.diplom.service.task2.BankAccountService;
import com.diplom.service.task2.DebitCardService;
import com.diplom.service.task3.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class LkController {
    private final CreditCardService creditCardService;
    private final DebitCardService debitCardService;
    private final BankAccountService bankAccountService;
    private final DepositService depositService;

    @Autowired
    public LkController(CreditCardService creditCardService,
                        DebitCardService debitCardService,
                        BankAccountService bankAccountService,
                        DepositService depositService) {
        this.creditCardService = creditCardService;
        this.debitCardService = debitCardService;
        this.bankAccountService = bankAccountService;
        this.depositService = depositService;
    }

    @GetMapping("/lk")
    public String getCreditCards(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        model.addAttribute("creditCards", creditCardService.getCreditCards(username));
        model.addAttribute("creditCardsInProcess", creditCardService.getCreditCardsInProcess(username));
        model.addAttribute("debitCards", debitCardService.findByUsername(username));
        model.addAttribute("bankAccounts", bankAccountService.findByUsername(username));
        model.addAttribute("deposits", depositService.findDepositEntitiesByUsername(username));
        return "lk";
    }

    @PostMapping("/lk")
    public String checkBid(@RequestParam(required = true, defaultValue = "") Long id,
                           @RequestParam(required = true, defaultValue = "") String action,
                           Model model) {
        if (action.equals("submit")) {
            creditCardService.submit(id);
        }
        if (action.equals("decline")) {
            creditCardService.decline(id);
        }
        if (action.equals("close")) {
            depositService.closeDepositEntityById(id);
        }
        if (action.equals("take")) {
            return "redirect:deposit?action=take&id=" + id.toString();
        }
        if (action.equals("fill")) {
            return "redirect:deposit?action=fill&id=" + id.toString();
        }
        return "redirect:/lk";
    }
}