package com.diplom.controller.task3;

import com.diplom.entity.task3.DepositEntity;
import com.diplom.service.task3.DepositOperationService;
import com.diplom.service.task3.DepositService;
import com.diplom.validator.task3.DepositOperationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@Controller
public class DepositController {
    @Autowired
    private DepositService depositService;
    @Autowired
    private DepositOperationService depositOperationService;
    @Autowired
    private DepositOperationValidator depositOperationValidator;

    @GetMapping("/deposit")
    public String userList(@RequestParam String action, @RequestParam String id, Model model) {
        DepositEntity depositEntity = depositService.findDepositEntityById(Long.valueOf(id));
        model.addAttribute("depositEntity", depositEntity);
        model.addAttribute("action", action);
        return "deposit";
    }

    @PostMapping("/deposit")
    public String addCredit(@RequestParam(required = true, defaultValue = "") String id,
                            @RequestParam(required = true, defaultValue = "") String sum,
                            @RequestParam(required = true, defaultValue = "") String action,
                            Model model) {

        BigDecimal operationSum = BigDecimal.valueOf(Long.parseLong(sum));

        if (depositOperationValidator.check(Long.valueOf(id), operationSum, action))
            return "redirect:/lk";
        depositOperationService.newDepositOperation(Long.valueOf(id), operationSum, action);
        return "redirect:/lk";
    }
}
