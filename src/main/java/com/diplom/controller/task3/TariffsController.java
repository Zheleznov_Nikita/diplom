package com.diplom.controller.task3;

import com.diplom.dto.task3.TariffDto;
import com.diplom.service.task3.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TariffsController {
    @Autowired
    private TariffService tariffService;

    @GetMapping("/admin/tariffs")
    public String tariffList(Model model) {
        model.addAttribute("allTariffs", tariffService.findAll());
        return "tariffs";
    }

    @PostMapping("/admin/tariffs")
    public String editTariff(@ModelAttribute("tariffFrom") TariffDto tariffDto,
                             @RequestParam(required = true, defaultValue = "") String action,
                             Model model) {
        if (action.equals("edit"))
            tariffService.saveFromDto(tariffDto);
        else
            tariffService.deleteFromDto(tariffDto);
        return "redirect:/admin/tariffs";
    }

    @GetMapping("/admin/newtariff")
    public String newTariff(Model model) {
        model.addAttribute("tariffFrom", new TariffDto());
        return "newtariff";
    }

    @PostMapping("/admin/newtariff")
    public String saveNewTariff(@ModelAttribute("tariffFrom") TariffDto tariffDto,
                                Model model) {
        tariffService.newTariffFromDto(tariffDto);
        return "redirect:/admin/tariffs";
    }
}
