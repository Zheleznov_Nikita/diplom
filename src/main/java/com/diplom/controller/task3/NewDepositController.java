package com.diplom.controller.task3;

import com.diplom.service.task3.DepositService;
import com.diplom.service.task3.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NewDepositController {
    @Autowired
    private TariffService tariffService;
    @Autowired
    private DepositService depositService;

    @GetMapping("/newdeposit")
    public String getTariffs(Model model) {
        model.addAttribute("allTariffs", tariffService.findAll());
        return "newdeposit";
    }

    @PostMapping("/newdeposit")
    public String addDeposit(@RequestParam(required = true, defaultValue = "") String id,
                             @RequestParam(required = true, defaultValue = "") String renewable,
                             @RequestParam(required = true, defaultValue = "") String balance,
                             Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        depositService.newDeposit(Long.valueOf(id), !renewable.equals(""), balance, username);
        return "redirect:/lk";
    }
}
