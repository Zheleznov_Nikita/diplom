package com.diplom.controller.task2;

import com.diplom.enums.task2.TransferStatus;
import com.diplom.service.task2.transfers.TransferFromAccountService;
import com.diplom.service.task2.transfers.TransferFromCardService;
import com.diplom.validator.task2.FraudTransferSaver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminTransfersController {

    private final TransferFromAccountService transferFromAccountService;
    private final TransferFromCardService transferFromCardService;
    private final FraudTransferSaver fraudTransferSaver;

    @Autowired
    public AdminTransfersController(TransferFromAccountService transferFromAccountService,
                                    TransferFromCardService transferFromCardService,
                                    FraudTransferSaver fraudTransferSaver) {
        this.transferFromAccountService = transferFromAccountService;
        this.transferFromCardService = transferFromCardService;
        this.fraudTransferSaver = fraudTransferSaver;
    }

    @GetMapping("/admin/transfers")
    public String transferList(Model model) {
        model.addAttribute("transferFromAccount", transferFromAccountService.findAllByStatus(TransferStatus.SUSPECT));
        model.addAttribute("transferFromCard", transferFromCardService.findAllByStatus(TransferStatus.SUSPECT));
        return "fraudtransfer";
    }

    @PostMapping("/admin/transfers")
    public String checkTransfers(@RequestParam(required = true, defaultValue = "") Long id,
                                 @RequestParam(required = true, defaultValue = "") String type,
                                 @RequestParam(required = true, defaultValue = "") String action,
                                 Model model) {
        fraudTransferSaver.update(id, type, action);
        return "redirect:/admin/transfers";
    }
}
