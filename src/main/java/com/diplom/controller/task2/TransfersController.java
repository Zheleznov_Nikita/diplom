package com.diplom.controller.task2;

import com.diplom.service.task2.transfers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TransfersController {
    private final AccountToAccountService accountToAccountService;
    private final AccountToCardService accountToCardService;
    private final CardToAccountService cardToAccountService;
    private final CardToCardService cardToCardService;
    private final TransferFromAccountService transferFromAccountService;
    private final TransferFromCardService transferFromCardService;

    @Autowired
    public TransfersController(AccountToAccountService accountToAccountService,
                               AccountToCardService accountToCardService,
                               CardToAccountService cardToAccountService,
                               CardToCardService cardToCardService,
                               TransferFromAccountService transferFromAccountService,
                               TransferFromCardService transferFromCardService) {
        this.accountToAccountService = accountToAccountService;
        this.accountToCardService = accountToCardService;
        this.cardToAccountService = cardToAccountService;
        this.cardToCardService = cardToCardService;
        this.transferFromAccountService = transferFromAccountService;
        this.transferFromCardService = transferFromCardService;
    }

    @GetMapping("/transfers")
    public String userList(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        //Transfers
        model.addAttribute("transferModel1", accountToAccountService.findTransfersByUsername(username));
        model.addAttribute("transferModel2", accountToCardService.findTransfersByUsername(username));
        model.addAttribute("transferModel3", cardToAccountService.findTransfersByUsername(username));
        model.addAttribute("transferModel4", cardToCardService.findTransfersByUsername(username));
        model.addAttribute("transferModel5", transferFromAccountService.findAllByUsername(username));
        model.addAttribute("transferModel6", transferFromCardService.findAllByUsername(username));
        //Enrollments
        model.addAttribute("enrollmentModel1", accountToAccountService.findEnrollmentsByUsername(username));
        model.addAttribute("enrollmentModel2", accountToCardService.findEnrollmentsByUsername(username));
        model.addAttribute("enrollmentModel3", cardToAccountService.findEnrollmentsByUsername(username));
        model.addAttribute("enrollmentModel4", cardToCardService.findEnrollmentsByUsername(username));
        //inner transactions
        model.addAttribute("innerTransferModel1", accountToAccountService.findInnerTransfers(username));
        model.addAttribute("innerTransferModel2", accountToCardService.findInnerTransfers(username));
        model.addAttribute("innerTransferModel3", cardToAccountService.findInnerTransfers(username));
        model.addAttribute("innerTransferModel4", cardToCardService.findInnerTransfers(username));
        return "transfers";
    }
}
