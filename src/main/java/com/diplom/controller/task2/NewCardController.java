package com.diplom.controller.task2;

import com.diplom.dto.task2.DebitCardDto;
import com.diplom.service.task2.DebitCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class NewCardController {
    @Autowired
    private DebitCardService debitCardService;

    @GetMapping("/newcard")
    public String newBid(Model model) {
        model.addAttribute("debitCardForm", new DebitCardDto());
        return "newcard";
    }

    @PostMapping("/newcard")
    public String addBid(@ModelAttribute("debitCardForm") DebitCardDto debitCardDto,
                         Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        debitCardService.saveFromDto(debitCardDto, username);
        return "redirect:/lk";
    }
}
