package com.diplom.controller.task2;

import com.diplom.dto.task2.BankAccountDto;
import com.diplom.service.task2.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class NewBankAccountController {
    @Autowired
    private BankAccountService bankAccountService;

    @GetMapping("/newbankaccount")
    public String newBid(Model model) {
        model.addAttribute("bankAccountForm", new BankAccountDto());
        return "newbankaccount";
    }

    @PostMapping("/newbankaccount")
    public String addBid(@ModelAttribute("bankAccountForm") BankAccountDto bankAccountDto,
                         Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        bankAccountService.saveFromDto(bankAccountDto, username);
        return "redirect:/lk";
    }
}
