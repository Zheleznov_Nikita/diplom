package com.diplom.controller.task2;

import com.diplom.dto.task2.TransferDto;
import com.diplom.service.task2.BankAccountService;
import com.diplom.service.task2.DebitCardService;
import com.diplom.validator.task2.TransferValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TransferByNumberController {
    private final BankAccountService bankAccountService;
    private final DebitCardService debitCardService;
    private final TransferValidator transferValidator;

    @Autowired
    public TransferByNumberController(BankAccountService bankAccountService, DebitCardService debitCardService, TransferValidator transferValidator) {
        this.bankAccountService = bankAccountService;
        this.debitCardService = debitCardService;
        this.transferValidator = transferValidator;
    }

    @GetMapping("/transfer/bynumber")
    public String userList(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        List<String> requisites = new ArrayList<>();
        requisites.addAll(bankAccountService.findAllAccountNumbersByUsername(username));
        requisites.addAll(debitCardService.findAllCardNumbersByUsername(username));
        model.addAttribute("requisitesModel", requisites);
        model.addAttribute("transferForm", new TransferDto());
        return "transferbynumber";
    }

    @PostMapping("/transfer/bynumber")
    public String addBid(@ModelAttribute("transferForm") TransferDto transferDto,
                         Model model) {
        transferValidator.checkTransfer(transferDto);
        return "redirect:/lk";
    }
}