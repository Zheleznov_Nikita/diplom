package com.diplom.service.general;

import com.diplom.dto.task1.UserDto;
import com.diplom.entity.general.ClientEntity;
import com.diplom.entity.general.RoleEntity;
import com.diplom.entity.general.UserEntity;
import com.diplom.repository.general.RoleRepository;
import com.diplom.repository.general.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username);

        if (userEntity == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return userEntity;
    }

    public UserEntity findUserById(Long userId) {
        Optional<UserEntity> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new UserEntity());
    }

    public List<UserEntity> allUsers() {
        return userRepository.findAll();
    }

    public boolean saveUser(UserEntity userEntity) {
        UserEntity userEntityFromDB = userRepository.findByUsername(userEntity.getUsername());
        if (userEntityFromDB != null) {
            return false;
        }
        userEntity.setRoles(Collections.singleton(new RoleEntity(1L, "ROLE_USER")));
        userEntity.setPassword(bCryptPasswordEncoder.encode(userEntity.getPassword()));
        userRepository.save(userEntity);
        return true;
    }

    public boolean saveAdminUser(UserEntity userEntity) {
        UserEntity userEntityFromDB = userRepository.findByUsername(userEntity.getUsername());

        if (userEntityFromDB != null) {
            return false;
        }

        userEntity.setRoles(Collections.singleton(new RoleEntity(2L, "ROLE_ADMIN")));
        userEntity.setPassword(bCryptPasswordEncoder.encode(userEntity.getPassword()));
        userRepository.save(userEntity);
        return true;
    }

    public boolean deleteUser(Long userId) {
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
    }

    public UserEntity newUser(UserDto userDto) {
        UserEntity userEntity = new UserEntity();
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setFirstname(userDto.getFirstname());
        clientEntity.setSurname(userDto.getSurname());
        clientEntity.setPatronymic(userDto.getPatronymic());
        clientEntity.setPhone(userDto.getPhone());
        userEntity.setUsername(userDto.getUsername());
        userEntity.setPassword(userDto.getPassword());
        userEntity.setPasswordConfirm(userDto.getPasswordConfirm());
        userEntity.setClientEntity(clientEntity);
        clientEntity.setUserEntity(userEntity);
        return userEntity;
    }
}
