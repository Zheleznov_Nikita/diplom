package com.diplom.service.general;

import com.diplom.entity.general.ClientEntity;
import com.diplom.repository.general.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public ClientEntity findClientByUsername(String username) {
        return clientRepository.findClientEntityByUsername(username);
    }
}
