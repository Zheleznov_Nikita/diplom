package com.diplom.service.task2;

import com.diplom.dto.task2.BankAccountDto;
import com.diplom.entity.task2.BankAccountEntity;
import com.diplom.repository.task2.BankAccountRepository;
import com.diplom.service.general.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class BankAccountService {
    @Autowired
    private BankAccountRepository bankAccountRepository;
    @Autowired
    private ClientService clientService;

    private String generateNumber(List<String> bankAccountNumbers) {
        Random random = new Random();
        int n1 = 1_000_000 + random.nextInt(9_000_000);
        int n2 = 1_000_000 + random.nextInt(9_000_000);
        String number = "111111" + n1 + n2;
        if (bankAccountNumbers.contains(number))
            generateNumber(bankAccountNumbers);
        return number;
    }

    public void saveFromDto(BankAccountDto bankAccountDto, String username) {
        BankAccountEntity bankAccountEntity = new BankAccountEntity();
        bankAccountEntity.setSum(bankAccountDto.getSum());
        bankAccountEntity.setClientEntity(clientService.findClientByUsername(username));
        bankAccountEntity.setAccountNumber(generateNumber(findAllAccountNumbers()));
        bankAccountRepository.save(bankAccountEntity);
    }

    public void save(BankAccountEntity bankAccountEntity) {
        bankAccountRepository.save(bankAccountEntity);
    }

    public List<String> findAllAccountNumbers() {
        return bankAccountRepository.findAllAccountNumbers();
    }

    public List<String> findAllAccountNumbersByUsername(String username) {
        return bankAccountRepository.findAllAccountNumbersByUsername(username);
    }

    public List<BankAccountEntity> findByUsername(String username) {
        return bankAccountRepository.findAllByUsername(username);
    }

    public BankAccountEntity findByAccountNumber(String accountNumber) {
        return bankAccountRepository.findByAccountNumber(accountNumber);
    }
}
