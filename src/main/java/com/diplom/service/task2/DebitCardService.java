package com.diplom.service.task2;

import com.diplom.dto.task2.DebitCardDto;
import com.diplom.entity.task2.DebitCardEntity;
import com.diplom.repository.task2.DebitCardRepository;
import com.diplom.service.general.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class DebitCardService {
    @Autowired
    private DebitCardRepository debitCardRepository;
    @Autowired
    private ClientService clientService;

    private String generateNumber(List<String> creditCards) {
        Random random = new Random();
        int n1 = 10_000 + random.nextInt(90_000);
        int n2 = 10_000 + random.nextInt(90_000);
        String number = "111111" + n1 + n2;
        if (creditCards.contains(number))
            generateNumber(creditCards);
        return number;
    }

    public void save(DebitCardEntity debitCardEntity) {
        debitCardRepository.save(debitCardEntity);
    }

    public void saveFromDto(DebitCardDto debitCardDto, String username) {
        DebitCardEntity debitCardEntity = new DebitCardEntity();
        debitCardEntity.setSum(debitCardDto.getSum());
        debitCardEntity.setClientEntity(clientService.findClientByUsername(username));
        debitCardEntity.setCardNumber(generateNumber(findAllCardNumbers()));
        debitCardRepository.save(debitCardEntity);
    }

    public List<String> findAllCardNumbers() {
        return debitCardRepository.findAllCardNumbers();
    }

    public List<String> findAllCardNumbersByUsername(String username) {
        return debitCardRepository.findAllCardNumbersByUsername(username);
    }

    public List<DebitCardEntity> findByUsername(String username) {
        return debitCardRepository.findAllByUsername(username);
    }

    public DebitCardEntity findByAccountNumber(String cardNumber) {
        return debitCardRepository.findByAccountNumber(cardNumber);
    }
}
