package com.diplom.service.task2.transfers;

import com.diplom.entity.task2.BankAccountEntity;
import com.diplom.entity.task2.transfers.TransferFromAccountEntity;
import com.diplom.enums.task2.TransferStatus;
import com.diplom.repository.task2.transfers.TransferFromAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class TransferFromAccountService {
    @Autowired
    private TransferFromAccountRepository transferRepository;

    public List<TransferFromAccountEntity> findAllByUsername(String username) {
        return transferRepository.findAllByUsername(username);
    }

    public int findCountByAccountNumber(BankAccountEntity bankAccountEntity, Date date) {
        return transferRepository.findCountByAccountNumber(bankAccountEntity, date);
    }

    public void save(TransferFromAccountEntity transferEntity) {
        transferRepository.save(transferEntity);
    }

    public List<TransferFromAccountEntity> findAllByStatus(TransferStatus status) {
        return transferRepository.findTransferFromAccountEntitiesByStatus(status);
    }

    public TransferFromAccountEntity findById(Long id) {
        return transferRepository.findTransferFromAccountEntityById(id);
    }
}
