package com.diplom.service.task2.transfers;

import com.diplom.entity.task2.transfers.AccountToAccountEntity;
import com.diplom.repository.task2.transfers.AccountToAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountToAccountService {
    @Autowired
    AccountToAccountRepository accountRepository;

    public List<AccountToAccountEntity> findTransfersByUsername(String username) {
        return accountRepository.findTransfersByUsername(username);
    }

    public List<AccountToAccountEntity> findEnrollmentsByUsername(String username) {
        return accountRepository.findEnrollmentsByUsername(username);
    }

    public List<AccountToAccountEntity> findInnerTransfers(String username) {
        return accountRepository.findInnerTransfers(username);
    }

    public void save(AccountToAccountEntity accountToAccountEntity) {
        accountRepository.save(accountToAccountEntity);
    }
}
