package com.diplom.service.task2.transfers;

import com.diplom.entity.task2.transfers.CardToAccountEntity;
import com.diplom.repository.task2.transfers.CardToAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardToAccountService {
    @Autowired
    private CardToAccountRepository cardRepository;

    public List<CardToAccountEntity> findTransfersByUsername(String username) {
        return cardRepository.findTransfersByUsername(username);
    }

    public List<CardToAccountEntity> findEnrollmentsByUsername(String username) {
        return cardRepository.findEnrollmentsByUsername(username);
    }

    public List<CardToAccountEntity> findInnerTransfers(String username) {
        return cardRepository.findInnerTransfers(username);
    }

    public void save(CardToAccountEntity transferEntity) {
        cardRepository.save(transferEntity);
    }
}
