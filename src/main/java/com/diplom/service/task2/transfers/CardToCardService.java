package com.diplom.service.task2.transfers;

import com.diplom.entity.task2.transfers.CardToCardEntity;
import com.diplom.repository.task2.transfers.CardToCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardToCardService {
    @Autowired
    private CardToCardRepository cardRepository;

    public List<CardToCardEntity> findTransfersByUsername(String username) {
        return cardRepository.findTransfersByUsername(username);
    }

    public List<CardToCardEntity> findEnrollmentsByUsername(String username) {
        return cardRepository.findEnrollmentsByUsername(username);
    }

    public List<CardToCardEntity> findInnerTransfers(String username) {
        return cardRepository.findInnerTransfers(username);
    }

    public void save(CardToCardEntity transferEntity) {
        cardRepository.save(transferEntity);
    }
}
