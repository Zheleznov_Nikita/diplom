package com.diplom.service.task2.transfers;

import com.diplom.entity.task2.DebitCardEntity;
import com.diplom.entity.task2.transfers.TransferFromCardEntity;
import com.diplom.enums.task2.TransferStatus;
import com.diplom.repository.task2.transfers.TransferFromCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class TransferFromCardService {
    @Autowired
    private TransferFromCardRepository transferRepository;

    public List<TransferFromCardEntity> findAllByUsername(String username) {
        return transferRepository.findAllByUsername(username);
    }

    public int findCountByAccountNumber(DebitCardEntity debitCardEntity, Date date) {
        return transferRepository.findCountByAccountNumber(debitCardEntity, date);
    }

    public void save(TransferFromCardEntity transferEntity) {
        transferRepository.save(transferEntity);
    }

    public List<TransferFromCardEntity> findAllByStatus(TransferStatus status) {
        return transferRepository.findTransferFromCardEntitiesByStatus(status);
    }

    public TransferFromCardEntity findById(Long id) {
        return transferRepository.findTransferFromCardEntityById(id);
    }
}
