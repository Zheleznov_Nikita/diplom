package com.diplom.service.task2.transfers;

import com.diplom.entity.task2.transfers.AccountToCardEntity;
import com.diplom.repository.task2.transfers.AccountToCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountToCardService {
    @Autowired
    private AccountToCardRepository accountRepository;

    public List<AccountToCardEntity> findTransfersByUsername(String username) {
        return accountRepository.findTransfersByUsername(username);
    }

    public List<AccountToCardEntity> findEnrollmentsByUsername(String username) {
        return accountRepository.findEnrollmentsByUsername(username);
    }

    public List<AccountToCardEntity> findInnerTransfers(String username) {
        return accountRepository.findInnerTransfers(username);
    }

    public void save(AccountToCardEntity transferEntity) {
        accountRepository.save(transferEntity);
    }
}
