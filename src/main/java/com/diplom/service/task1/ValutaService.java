package com.diplom.service.task1;

import com.diplom.entity.task1.ValutaEntity;
import com.diplom.repository.task1.ValutaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ValutaService {

    @Autowired
    private ValutaRepository valutaRepository;

    public List<ValutaEntity> getAll() {
        return valutaRepository.findAll();
    }

    public ValutaEntity getByName(String name) {
        return valutaRepository.findValutaEntityByName(name);
    }
}
