package com.diplom.service.task1;

import com.diplom.dto.task1.CommissionDto;
import com.diplom.entity.general.CommissionEntity;
import com.diplom.repository.task1.CommissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommissionService {
    @Autowired
    private CommissionRepository commissionRepository;

    public CommissionEntity findById(Long id) {
        return commissionRepository.findCommissionEntityById(id);
    }

    public void save(CommissionDto commissionDto) {
        CommissionEntity commissionEntity = new CommissionEntity();
        commissionEntity.setCommission(commissionDto.getCommission());
        commissionEntity.setPeni(commissionDto.getPeni());
        commissionEntity.setTransferCommission(commissionDto.getTransferCommission());
        commissionEntity.setId(commissionDto.getId());
        commissionRepository.save(commissionEntity);
    }
}
