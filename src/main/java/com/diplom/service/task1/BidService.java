package com.diplom.service.task1;

import com.diplom.dto.task1.BidDto;
import com.diplom.entity.task1.BidEntity;
import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.enums.task1.BidStatus;
import com.diplom.enums.task1.CardStatus;
import com.diplom.repository.task1.BidRepository;
import com.diplom.service.general.ClientService;
import com.diplom.validator.task1.NewCreditValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class BidService {

    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private CreditCardService creditCardService;
    @Autowired
    private NewCreditValidator newCreditValidator;
    @Autowired
    private ClientService clientService;
    @Autowired
    private ValutaService valutaService;

    public void saveBid(BidEntity bidEntity) {
        bidRepository.save(bidEntity);
    }

    public List<BidEntity> getAll() {
        return bidRepository.findAll();
    }

    public Optional<BidEntity> getById(Long bidId) {
        return bidRepository.findById(bidId);
    }

    public void submit(Long bidId, double percent, Date endDate) {
        BidEntity bidEntity = bidRepository.findBidEntityById(bidId);
        bidEntity.setBidStatus(BidStatus.APPROVED);
        bidRepository.save(bidEntity);
        CreditCardEntity creditCardEntity = new CreditCardEntity();
        creditCardEntity.setSumLimit(bidEntity.getSumLimit());
        creditCardEntity.setClientEntity(bidEntity.getClientEntity());
        creditCardEntity.setCardStatus(CardStatus.IN_PROCESS);
        creditCardEntity.setEndDate(endDate);
        creditCardEntity.setPercent(percent);
        creditCardEntity.setValutaEntity(bidEntity.getValutaEntity());
        creditCardService.save(creditCardEntity);
    }

    public void decline(Long bidId) {
        BidEntity bidEntity = bidRepository.findBidEntityById(bidId);
        bidEntity.setBidStatus(BidStatus.DECLINED);
        bidRepository.save(bidEntity);
    }

    public void newBid(BidDto bidDto, String username) {
        BidEntity bidEntity = new BidEntity();
        bidEntity.setClientEntity(clientService.findClientByUsername(username));
        bidEntity.setSumLimit(bidDto.getSumLimit());
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        bidEntity.setBidDate(date);
        bidEntity.setBidStatus(BidStatus.IN_PROCESS);
        bidEntity.setValutaEntity(valutaService.getByName(bidDto.getValuta()));
        if (newCreditValidator.checkCreditCardSum(bidEntity, username))
            saveBid(bidEntity);
    }

    public List<BidEntity> findInProgress() {
        return bidRepository.findBidEntitiesByBidStatusIs(BidStatus.IN_PROCESS);
    }

    public List<BidEntity> findByUsername(String username) {
        return bidRepository.findBidEntitiesByUsername(username);
    }
}
