package com.diplom.service.task1;

import com.diplom.dto.task1.PaymentDto;
import com.diplom.entity.task1.PaymentEntity;
import com.diplom.repository.task1.PaymentRepository;
import com.diplom.validator.task1.SumPaymentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private SumPaymentValidator sumPaymentValidator;
    @Autowired
    private CreditService creditService;

    public BigDecimal findSumPaymentsByCreditId(Long id, String username) {
        return paymentRepository.findSumPaymentsByCreditId(id, username);
    }

    public void save(PaymentEntity paymentEntity) {
        paymentRepository.save(paymentEntity);
    }

    public void newPayment(PaymentDto paymentDto) {
        PaymentEntity paymentEntity = new PaymentEntity();
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        paymentEntity.setSum(paymentDto.getSum());
        paymentEntity.setPaymentDate(date);
        paymentEntity.setCreditEntity(paymentDto.getCreditEntity());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        if (sumPaymentValidator.checkPaymentSum(paymentEntity.getSum(), paymentEntity.getCreditEntity().getId(), username))
            save(paymentEntity);
    }
}
