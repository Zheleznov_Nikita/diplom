package com.diplom.service.task1;

import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.enums.task1.CardStatus;
import com.diplom.repository.task1.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditCardService {
    @Autowired
    private CreditCardRepository creditCardRepository;

    public List<CreditCardEntity> getCreditCards(String userName) {
        return creditCardRepository.findCreditCardEntitiesByUsername(userName, CardStatus.OPEN, CardStatus.EXPIRED);
    }

    public List<CreditCardEntity> getCreditCardsInProcess(String userName) {
        return creditCardRepository.findCreditCardEntitiesByUsernameInProcess(userName, CardStatus.IN_PROCESS);
    }

    public void save(CreditCardEntity creditCardEntity) {
        creditCardRepository.save(creditCardEntity);
    }

    public void submit(Long id) {
        CreditCardEntity creditCardEntity = creditCardRepository.findCreditCardEntityById(id);
        creditCardEntity.setCardStatus(CardStatus.OPEN);
        creditCardRepository.save(creditCardEntity);

    }

    public void decline(Long id) {
        CreditCardEntity creditCardEntity = creditCardRepository.findCreditCardEntityById(id);
        creditCardEntity.setCardStatus(CardStatus.DECLINED);
        creditCardRepository.save(creditCardEntity);
    }

    public CreditCardEntity findById(Long id) {
        return creditCardRepository.findCreditCardEntityById(id);
    }

    public CreditCardEntity findByUsernameAndId(String username, Long id) {
        return creditCardRepository.findCreditCardEntitiesByUsernameAndId(username, id);
    }

    public List<CreditCardEntity> findAll() {
        return creditCardRepository.findAll();
    }

}
