package com.diplom.service.task1;

import com.diplom.dto.task1.CreditDto;
import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.entity.task1.CreditEntity;
import com.diplom.entity.task1.ValutaEntity;
import com.diplom.enums.task1.CreditStatus;
import com.diplom.repository.task1.CreditRepository;
import com.diplom.validator.task1.NewCreditValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

@Service
public class CreditService {
    @Autowired
    private CreditRepository creditRepository;
    @Autowired
    private CreditService creditService;
    @Autowired
    private CreditCardService creditCardService;
    @Autowired
    private NewCreditValidator newCreditValidator;

    public List<CreditEntity> findByCreditCardId(CreditCardEntity creditCardEntity) {
        return creditRepository.findCreditEntityByCreditCardEntity(creditCardEntity, CreditStatus.REPAID);
    }

    public void save(CreditEntity creditEntity) {
        creditRepository.save(creditEntity);
    }

    public BigDecimal findAllCreditSum(Long id, String username) {
        return creditRepository.findAllCreditSum(id, username, CreditStatus.REPAID);
    }

    public BigDecimal findCreditSumByIdAndUsername(Long id, String username) {
        return creditRepository.findCreditSumByIdAndUsername(id, username);
    }

    public ValutaEntity findValutaByCreditIdAndUsername(Long id, String username) {
        return creditRepository.findValutaByCreditIdAndUsername(id, username);
    }

    public CreditEntity findCreditEntityByIdAndUsername(Long id, String username) {
        return creditRepository.findCreditEntityByIdAndUsername(id, username);
    }

    public int findCountByExpiredTerm(String username) {
        return creditRepository.findCountByCreditStatusAndUsername(username, CreditStatus.EXPIRED);
    }

    public List<CreditEntity> findAllByTerm(CreditStatus creditStatus) {
        return creditRepository.findAllByCreditStatus(creditStatus);
    }

    public int findCountByCreditCardIdAndStatus(Long id, CreditStatus creditStatus) {
        return creditRepository.findCountByCreditCardIdAndStatus(id, creditStatus);
    }

    public void newCredit(CreditDto creditDto, String username) {
        CreditEntity creditEntity = new CreditEntity();
        creditEntity.setSum(creditDto.getSumCredit());
        CreditCardEntity creditCardEntity = creditCardService.findById(creditDto.getCreditCardId());
        creditEntity.setCreditCardEntity(creditCardEntity);
        creditEntity.setPenalization(0);
        creditEntity.setCreditStatus(CreditStatus.OPENED);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        creditEntity.setStartDate(date);
        creditEntity = newCreditValidator.checkDate(creditEntity);
        if (creditEntity != null && newCreditValidator.checkSum(creditEntity, creditCardEntity.getId(), username))
            creditService.save(creditEntity);
    }
}
