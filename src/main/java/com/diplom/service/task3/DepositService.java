package com.diplom.service.task3;

import com.diplom.entity.task3.DepositEntity;
import com.diplom.entity.task3.TariffEntity;
import com.diplom.enums.task3.DepositStatus;
import com.diplom.repository.task3.DepositRepository;
import com.diplom.service.general.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Service
public class DepositService {
    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private TariffService tariffService;
    @Autowired
    private ClientService clientService;

    public List<DepositEntity> findDepositEntitiesByAutoRenewable(boolean auto) {
        return depositRepository.findDepositEntitiesByAutoRenewable(auto);
    }

    public void save(DepositEntity depositEntity) {
        depositRepository.save(depositEntity);
    }

    public List<DepositEntity> findDepositEntitiesByUsername(String username) {
        return depositRepository.findDepositEntitiesByUsername(username, DepositStatus.OPEN);
    }

    public void closeDepositEntityById(Long id) {
        DepositEntity depositEntity = depositRepository.findDepositEntityById(id);
        depositEntity.setStatus(DepositStatus.CLOSED);
        save(depositEntity);
    }

    public DepositEntity findDepositEntityById(Long id) {
        return depositRepository.findDepositEntityById(id);
    }

    //поиск всех открытых вкладов, которые были открыты более месяца назад в это же число
    public List<DepositEntity> findDepositEntitiesByStatus(DepositStatus status, int day, Date today) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        java.sql.Timestamp timestamp = java.sql.Timestamp.valueOf(dateFormat.format(today));
        return depositRepository.findDepositEntitiesByStatus(status.toString(), day, timestamp);
    }

    public void newDeposit(Long id, boolean renewable, String balance, String username) {
        DepositEntity depositEntity = new DepositEntity();
        TariffEntity tariffEntity = tariffService.findById(id);
        depositEntity.setTariffEntity(tariffEntity);
        depositEntity.setStartBalance(BigDecimal.valueOf(Long.parseLong(balance)));
        depositEntity.setBalance(BigDecimal.valueOf(Long.parseLong(balance)));
        depositEntity.setAutoRenewable(renewable);
        depositEntity.setStatus(DepositStatus.OPEN);
        Calendar calendar = Calendar.getInstance();
        Date todayDate = new Date(Calendar.getInstance().getTime().getTime());
        depositEntity.setStartDate(todayDate);
        calendar.setTime(todayDate);
        calendar.add(Calendar.MONTH, tariffEntity.getTerm());
        depositEntity.setEndDate(new Date(calendar.getTime().getTime()));
        depositEntity.setClientEntity(clientService.findClientByUsername(username));
        save(depositEntity);
    }
}
