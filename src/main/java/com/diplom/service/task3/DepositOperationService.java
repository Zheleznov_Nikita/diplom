package com.diplom.service.task3;

import com.diplom.entity.task3.DepositEntity;
import com.diplom.entity.task3.DepositOperationEntity;
import com.diplom.enums.task3.OperationType;
import com.diplom.repository.task3.DepositOperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;

@Service
public class DepositOperationService {
    @Autowired
    private DepositOperationRepository depositOperationRepository;
    @Autowired
    private DepositService depositService;

    public int countAllByDateBetween(Date start, Date end) {
        return depositOperationRepository.countAllByDateBetween(start, end);
    }

    public DepositOperationEntity findFirstByDateBetween(Date start, Date end) {
        return depositOperationRepository.findFirstByDateBetween(start, end);
    }

    public DepositOperationEntity findMinByDate(Date start, Date end, DepositEntity depositEntity) {
        return depositOperationRepository.findMinByDate(start, end, depositEntity, OperationType.FROM_BANK);
    }

    public BigDecimal findSumByType(OperationType operationType, DepositEntity depositEntity) {
        return depositOperationRepository.findSumByType(operationType, depositEntity);
    }

    public void save(DepositOperationEntity depositOperationEntity) {
        depositOperationRepository.save(depositOperationEntity);
    }

    public void newDepositOperation(Long id, BigDecimal operationSum, String action) {
        DepositOperationEntity depositOperationEntity = new DepositOperationEntity();
        DepositEntity depositEntity = depositService.findDepositEntityById(id);
        depositOperationEntity.setSumOperation(operationSum);
        if (action.equals(OperationType.TAKE.toString())) {
            depositOperationEntity.setOperationType(OperationType.TAKE);
            depositOperationEntity.setMinDepositBalance(depositEntity.getBalance().subtract(operationSum));
            depositEntity.setBalance(depositEntity.getBalance().subtract(operationSum));
        } else {
            depositOperationEntity.setMinDepositBalance(depositEntity.getBalance());
            depositOperationEntity.setOperationType(OperationType.FILL);
            depositEntity.setBalance(depositEntity.getBalance().add(operationSum));
        }
        depositOperationEntity.setDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        depositOperationEntity.setDepositEntity(depositEntity);
        save(depositOperationEntity);
        depositService.save(depositEntity);
    }
}
