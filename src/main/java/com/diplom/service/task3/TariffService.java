package com.diplom.service.task3;

import com.diplom.dto.task3.TariffDto;
import com.diplom.entity.task3.TariffEntity;
import com.diplom.repository.task3.TariffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TariffService {
    @Autowired
    private TariffRepository tariffRepository;

    public List<TariffEntity> findAll() {
        return tariffRepository.findAll();
    }

    public void save(TariffEntity tariffEntity) {
        tariffRepository.save(tariffEntity);
    }

    public void saveFromDto(TariffDto tariffDto) {
        TariffEntity tariffEntity = tariffEntityFromDto(tariffDto);
        tariffEntity.setId(tariffDto.getId());
        tariffRepository.save(tariffEntity);
    }

    public void newTariffFromDto(TariffDto tariffDto) {
        tariffRepository.save(tariffEntityFromDto(tariffDto));
    }

    public TariffEntity tariffEntityFromDto(TariffDto tariffDto) {
        TariffEntity tariffEntity = new TariffEntity();
        tariffEntity.setName(tariffDto.getName());
        tariffEntity.setMinSum(tariffDto.getMinSum());
        tariffEntity.setMaxSum(tariffDto.getMaxSum());
        tariffEntity.setPercent(tariffDto.getPercent());
        tariffEntity.setTerm(tariffDto.getTerm());
        tariffEntity.setCapitalization(tariffDto.isCapitalization());
        tariffEntity.setClose(tariffDto.isClose());
        tariffEntity.setTake(tariffDto.isTake());
        tariffEntity.setRefill(tariffDto.isRefill());
        return tariffEntity;
    }

    public void delete(TariffEntity tariffEntity) {
        tariffRepository.delete(tariffEntity);
    }

    public void deleteFromDto(TariffDto tariffDto) {
        TariffEntity tariffEntity = tariffEntityFromDto(tariffDto);
        tariffEntity.setId(tariffDto.getId());
        tariffRepository.delete(tariffEntity);
    }

    public TariffEntity findById(Long id) {
        return tariffRepository.findTariffEntityById(id);
    }
}
