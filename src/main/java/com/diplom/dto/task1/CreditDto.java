package com.diplom.dto.task1;

import com.diplom.entity.task1.CreditCardEntity;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class CreditDto {
    private CreditCardEntity creditCardEntity;
    private Long creditCardId;
    @DecimalMin(value = "1000.0", message = "не менее 1000")
    private BigDecimal sumCredit;

    public CreditCardEntity getCreditCardEntity() {
        return creditCardEntity;
    }

    public void setCreditCardEntity(CreditCardEntity creditCardEntity) {
        this.creditCardEntity = creditCardEntity;
    }

    public BigDecimal getSumCredit() {
        return sumCredit;
    }

    public void setSumCredit(BigDecimal sumCredit) {
        this.sumCredit = sumCredit;
    }

    public Long getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(Long creditCardId) {
        this.creditCardId = creditCardId;
    }
}
