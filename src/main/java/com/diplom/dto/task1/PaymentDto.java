package com.diplom.dto.task1;

import com.diplom.entity.task1.CreditEntity;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.sql.Date;

public class PaymentDto {
    private Long id;
    @DecimalMin(value = "0.0", message = "не менее 0")
    private BigDecimal sum;
    private Date paymentDate;
    private CreditEntity creditEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public CreditEntity getCreditEntity() {
        return creditEntity;
    }

    public void setCreditEntity(CreditEntity creditEntity) {
        this.creditEntity = creditEntity;
    }
}
