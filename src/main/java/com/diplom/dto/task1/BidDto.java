package com.diplom.dto.task1;

import java.math.BigDecimal;

public class BidDto {
    private BigDecimal sumLimit;

    private String valuta;

    public String getValuta() {
        return valuta;
    }

    public void setValuta(String valuta) {
        this.valuta = valuta;
    }

    public BigDecimal getSumLimit() {
        return sumLimit;
    }

    public void setSumLimit(BigDecimal sumLimit) {
        this.sumLimit = sumLimit;
    }
}
