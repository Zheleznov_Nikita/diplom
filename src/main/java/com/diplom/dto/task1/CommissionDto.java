package com.diplom.dto.task1;

public class CommissionDto {
    private Long id;
    private double peni;
    private double commission;
    private double transferCommission;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getPeni() {
        return peni;
    }

    public void setPeni(double peni) {
        this.peni = peni;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public double getTransferCommission() {
        return transferCommission;
    }

    public void setTransferCommission(double transferCommission) {
        this.transferCommission = transferCommission;
    }
}
