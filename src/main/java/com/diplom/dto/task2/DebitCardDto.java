package com.diplom.dto.task2;

import com.diplom.entity.general.ClientEntity;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class DebitCardDto {
    private Long id;
    private String cardNumber;
    @DecimalMin("0.0")
    private BigDecimal sum;
    private ClientEntity clientEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public ClientEntity getClientEntity() {
        return clientEntity;
    }

    public void setClientEntity(ClientEntity clientEntity) {
        this.clientEntity = clientEntity;
    }
}
