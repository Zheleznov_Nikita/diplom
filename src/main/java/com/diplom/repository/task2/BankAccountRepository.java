package com.diplom.repository.task2;

import com.diplom.entity.task2.BankAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccountEntity, Long> {

    @Query("SELECT b.accountNumber " +
            "FROM BankAccountEntity b")
    List<String> findAllAccountNumbers();

    @Query("SELECT b " +
            "FROM BankAccountEntity b " +
            "WHERE b.clientEntity.userEntity.username = ?1")
    List<BankAccountEntity> findAllByUsername(String username);

    @Query("SELECT b.accountNumber " +
            "FROM BankAccountEntity b " +
            "WHERE b.clientEntity.userEntity.username = ?1")
    List<String> findAllAccountNumbersByUsername(String username);

    @Query("SELECT b " +
            "FROM BankAccountEntity b " +
            "WHERE b.accountNumber = ?1")
    BankAccountEntity findByAccountNumber(String accountNumber);
}
