package com.diplom.repository.task2.transfers;

import com.diplom.entity.task2.DebitCardEntity;
import com.diplom.entity.task2.transfers.TransferFromCardEntity;
import com.diplom.enums.task2.TransferStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface TransferFromCardRepository extends JpaRepository<TransferFromCardEntity, Long> {

    @Query("SELECT t " +
            "FROM TransferFromCardEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username = ?1")
    List<TransferFromCardEntity> findAllByUsername(String username);

    @Query("SELECT count(t) " +
            "FROM TransferFromCardEntity t " +
            "WHERE t.sourceEntity = ?1 and t.transferDate = ?2")
    int findCountByAccountNumber(DebitCardEntity debitCardEntity, Date date);

    List<TransferFromCardEntity> findTransferFromCardEntitiesByStatus(TransferStatus status);

    TransferFromCardEntity findTransferFromCardEntityById(Long id);
}
