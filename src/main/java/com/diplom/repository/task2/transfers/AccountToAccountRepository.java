package com.diplom.repository.task2.transfers;

import com.diplom.entity.task2.transfers.AccountToAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountToAccountRepository extends JpaRepository<AccountToAccountEntity, Long> {

    @Query("SELECT t " +
            "FROM AccountToAccountEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username = ?1 and t.recipientEntity.clientEntity.userEntity.username <> ?1")
    List<AccountToAccountEntity> findTransfersByUsername(String username);

    @Query("SELECT t " +
            "FROM AccountToAccountEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username <> ?1 and t.recipientEntity.clientEntity.userEntity.username = ?1")
    List<AccountToAccountEntity> findEnrollmentsByUsername(String username);

    @Query("SELECT t " +
            "FROM AccountToAccountEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username = ?1 and t.recipientEntity.clientEntity.userEntity.username = ?1")
    List<AccountToAccountEntity> findInnerTransfers(String username);
}
