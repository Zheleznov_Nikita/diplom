package com.diplom.repository.task2.transfers;

import com.diplom.entity.task2.BankAccountEntity;
import com.diplom.entity.task2.transfers.TransferFromAccountEntity;
import com.diplom.enums.task2.TransferStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface TransferFromAccountRepository extends JpaRepository<TransferFromAccountEntity, Long> {

    @Query("SELECT t " +
            "FROM TransferFromAccountEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username = ?1")
    List<TransferFromAccountEntity> findAllByUsername(String username);

    @Query("SELECT count(t) " +
            "FROM TransferFromAccountEntity t " +
            "WHERE t.sourceEntity = ?1 and t.transferDate = ?2")
    int findCountByAccountNumber(BankAccountEntity bankAccountEntity, Date date);

    List<TransferFromAccountEntity> findTransferFromAccountEntitiesByStatus(TransferStatus transferStatus);

    TransferFromAccountEntity findTransferFromAccountEntityById(Long id);
}
