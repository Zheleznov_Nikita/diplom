package com.diplom.repository.task2.transfers;

import com.diplom.entity.task2.transfers.CardToAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardToAccountRepository extends JpaRepository<CardToAccountEntity, Long> {

    @Query("SELECT t " +
            "FROM CardToAccountEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username = ?1 and t.recipientEntity.clientEntity.userEntity.username <> ?1")
    List<CardToAccountEntity> findTransfersByUsername(String username);

    @Query("SELECT t " +
            "FROM CardToAccountEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username <> ?1 and t.recipientEntity.clientEntity.userEntity.username = ?1")
    List<CardToAccountEntity> findEnrollmentsByUsername(String username);

    @Query("SELECT t " +
            "FROM CardToAccountEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username = ?1 and t.recipientEntity.clientEntity.userEntity.username = ?1")
    List<CardToAccountEntity> findInnerTransfers(String username);
}
