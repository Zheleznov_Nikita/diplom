package com.diplom.repository.task2.transfers;

import com.diplom.entity.task2.transfers.CardToCardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardToCardRepository extends JpaRepository<CardToCardEntity, Long> {

    @Query("SELECT t " +
            "FROM CardToCardEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username = ?1 and t.recipientEntity.clientEntity.userEntity.username <> ?1")
    List<CardToCardEntity> findTransfersByUsername(String username);

    @Query("SELECT t " +
            "FROM CardToCardEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username <> ?1 and t.recipientEntity.clientEntity.userEntity.username = ?1")
    List<CardToCardEntity> findEnrollmentsByUsername(String username);

    @Query("SELECT t " +
            "FROM CardToCardEntity t " +
            "WHERE t.sourceEntity.clientEntity.userEntity.username = ?1 and t.recipientEntity.clientEntity.userEntity.username = ?1")
    List<CardToCardEntity> findInnerTransfers(String username);
}
