package com.diplom.repository.task2;

import com.diplom.entity.task2.DebitCardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DebitCardRepository extends JpaRepository<DebitCardEntity, Long> {

    @Query("SELECT d.cardNumber " +
            "FROM DebitCardEntity d")
    List<String> findAllCardNumbers();

    @Query("SELECT d " +
            "FROM DebitCardEntity d " +
            "WHERE d.clientEntity.userEntity.username = ?1")
    List<DebitCardEntity> findAllByUsername(String username);

    @Query("SELECT d.cardNumber " +
            "FROM DebitCardEntity d " +
            "WHERE d.clientEntity.userEntity.username = ?1")
    List<String> findAllCardNumbersByUsername(String username);

    @Query("SELECT d " +
            "FROM DebitCardEntity d " +
            "WHERE d.cardNumber = ?1")
    DebitCardEntity findByAccountNumber(String cardNumber);
}
