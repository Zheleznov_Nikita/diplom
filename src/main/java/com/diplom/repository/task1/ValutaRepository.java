package com.diplom.repository.task1;

import com.diplom.entity.task1.ValutaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValutaRepository extends JpaRepository<ValutaEntity, Long> {
    ValutaEntity findValutaEntityByName(String name);
}
