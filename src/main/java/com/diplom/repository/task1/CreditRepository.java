package com.diplom.repository.task1;

import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.entity.task1.CreditEntity;
import com.diplom.entity.task1.ValutaEntity;
import com.diplom.enums.task1.CreditStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface CreditRepository extends JpaRepository<CreditEntity, Long> {

    @Query("SELECT cr " +
            "FROM CreditEntity cr " +
            "where cr.creditCardEntity = ?1 and cr.creditStatus <> ?2")
    List<CreditEntity> findCreditEntityByCreditCardEntity(CreditCardEntity creditCardEntity, CreditStatus creditStatus);

    @Query("SELECT cr " +
            "FROM CreditEntity cr " +
            "where cr.id = ?1 and cr.creditCardEntity.clientEntity.userEntity.username = ?2")
    CreditEntity findCreditEntityByIdAndUsername(Long id, String username);

    @Query("SELECT sum(cr.sum)" +
            "FROM CreditEntity cr " +
            "where cr.creditCardEntity.id = ?1 and cr.creditCardEntity.clientEntity.userEntity.username = ?2 " +
            "and cr.creditStatus <> ?3")
    BigDecimal findAllCreditSum(Long id, String username, CreditStatus creditStatus);

    @Query("SELECT cr.sum " +
            "FROM CreditEntity cr " +
            "where cr.id = ?1 and cr.creditCardEntity.clientEntity.userEntity.username = ?2")
    BigDecimal findCreditSumByIdAndUsername(Long id, String username);

    @Query("SELECT cr.creditCardEntity.valutaEntity " +
            "FROM CreditEntity cr " +
            "where cr.id = ?1 and cr.creditCardEntity.clientEntity.userEntity.username = ?2")
    ValutaEntity findValutaByCreditIdAndUsername(Long id, String username);

    @Query("SELECT count(cr)" +
            "FROM CreditEntity cr " +
            "where cr.creditCardEntity.clientEntity.userEntity.username = ?1 " +
            "and cr.creditStatus = ?2")
    int findCountByCreditStatusAndUsername(String username, CreditStatus creditStatus);

    @Query("SELECT cr " +
            "FROM CreditEntity cr " +
            "where cr.creditStatus = ?1")
    List<CreditEntity> findAllByCreditStatus(CreditStatus creditStatus);

    @Query("SELECT count(cr)" +
            "FROM CreditEntity cr " +
            "where cr.creditCardEntity.id = ?1 " +
            "and cr.creditStatus <> ?2")
    int findCountByCreditCardIdAndStatus(Long id, CreditStatus creditStatus);
}
