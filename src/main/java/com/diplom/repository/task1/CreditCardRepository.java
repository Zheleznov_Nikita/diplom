package com.diplom.repository.task1;

import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.enums.task1.CardStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreditCardRepository extends JpaRepository<CreditCardEntity, Long> {
    @Query("SELECT cr " +
            "FROM CreditCardEntity cr " +
            "JOIN ClientEntity cl " +
            "on cr.clientEntity.id = cl.id " +
            "JOIN UserEntity u " +
            "on cl.userEntity.id = u.id " +
            "WHERE u.username = ?1 and (cr.cardStatus = ?2 or cr.cardStatus = ?3)")
    List<CreditCardEntity> findCreditCardEntitiesByUsername(String userName, CardStatus cardStatus1, CardStatus cardStatus2);

    @Query("SELECT cr " +
            "FROM CreditCardEntity cr " +
            "JOIN ClientEntity cl " +
            "on cr.clientEntity.id = cl.id " +
            "JOIN UserEntity u " +
            "on cl.userEntity.id = u.id " +
            "WHERE u.username = ?1 and cr.cardStatus = ?2")
    List<CreditCardEntity> findCreditCardEntitiesByUsernameInProcess(String userName, CardStatus cardStatus);

    CreditCardEntity findCreditCardEntityById(Long id);

    @Query("SELECT cr " +
            "FROM CreditCardEntity cr " +
            "JOIN ClientEntity cl " +
            "on cr.clientEntity.id = cl.id " +
            "JOIN UserEntity u " +
            "on cl.userEntity.id = u.id " +
            "WHERE u.username = ?1 and cr.id = ?2")
    CreditCardEntity findCreditCardEntitiesByUsernameAndId(String userName, Long id);
}