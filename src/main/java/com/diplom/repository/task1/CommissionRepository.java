package com.diplom.repository.task1;

import com.diplom.entity.general.CommissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommissionRepository extends JpaRepository<CommissionEntity, Long> {
    CommissionEntity findCommissionEntityById(Long id);
}
