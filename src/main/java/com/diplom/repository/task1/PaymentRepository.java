package com.diplom.repository.task1;

import com.diplom.entity.task1.PaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;

public interface PaymentRepository extends JpaRepository<PaymentEntity, Long> {
    @Query("SELECT sum(p.sum)" +
            "FROM PaymentEntity p " +
            "where p.creditEntity.id = ?1 and p.creditEntity.creditCardEntity.clientEntity.userEntity.username = ?2")
    BigDecimal findSumPaymentsByCreditId(Long id, String username);
}