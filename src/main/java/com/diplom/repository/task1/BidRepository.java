package com.diplom.repository.task1;

import com.diplom.entity.task1.BidEntity;
import com.diplom.enums.task1.BidStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BidRepository extends JpaRepository<BidEntity, Long> {
    BidEntity findBidEntityById(Long bidId);

    List<BidEntity> findBidEntitiesByBidStatusIs(BidStatus bidStatus);

    @Query("SELECT b " +
            "FROM BidEntity b " +
            "JOIN ClientEntity cl " +
            "on b.clientEntity.id = cl.id " +
            "JOIN UserEntity u " +
            "on cl.userEntity.id = u.id " +
            "WHERE u.username = ?1")
    List<BidEntity> findBidEntitiesByUsername(String username);
}
