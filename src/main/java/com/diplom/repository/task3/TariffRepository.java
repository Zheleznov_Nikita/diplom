package com.diplom.repository.task3;

import com.diplom.entity.task3.TariffEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TariffRepository extends JpaRepository<TariffEntity, Long> {
    TariffEntity findTariffEntityById(Long id);
}
