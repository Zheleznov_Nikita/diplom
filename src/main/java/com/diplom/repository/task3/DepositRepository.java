package com.diplom.repository.task3;

import com.diplom.entity.task3.DepositEntity;
import com.diplom.enums.task3.DepositStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface DepositRepository extends JpaRepository<DepositEntity, Long> {
    List<DepositEntity> findDepositEntitiesByAutoRenewable(boolean auto);

    @Query("SELECT d " +
            "FROM DepositEntity d " +
            "WHERE d.clientEntity.userEntity.username = ?1 and d.status = ?2")
    List<DepositEntity> findDepositEntitiesByUsername(String username, DepositStatus status);

    DepositEntity findDepositEntityById(Long id);

    //поиск всех открытых вкладов, которые были открыты более месяца назад в это же число
    @Query(value = "select * from deposit " +
            "where status = ?1 and " +
            "DATE_PART('day', start_date) = ?2 " +
            "and ?3 - start_date >= 1", nativeQuery = true)
    List<DepositEntity> findDepositEntitiesByStatus(String status, int day, Timestamp today);
}
