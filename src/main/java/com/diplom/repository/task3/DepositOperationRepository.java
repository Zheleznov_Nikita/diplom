package com.diplom.repository.task3;

import com.diplom.entity.task3.DepositEntity;
import com.diplom.entity.task3.DepositOperationEntity;
import com.diplom.enums.task3.OperationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Date;

@Repository
public interface DepositOperationRepository extends JpaRepository<DepositOperationEntity, Long> {

    int countAllByDateBetween(Date start, Date end);

    DepositOperationEntity findFirstByDateBetween(Date start, Date end);

    @Query("SELECT d " +
            "FROM DepositOperationEntity d " +
            "WHERE d.minDepositBalance = (SELECT min(d1.minDepositBalance) " +
            "FROM DepositOperationEntity d1 " +
            "WHERE (d.date between ?1 and ?2) and d.depositEntity = ?3 and d.operationType <> ?4)")
    DepositOperationEntity findMinByDate(Date start, Date end, DepositEntity depositEntity, OperationType operationType);

    @Query("SELECT sum(d.sumOperation) " +
            "FROM DepositOperationEntity d " +
            "WHERE d.operationType = ?1 and d.depositEntity = ?2")
    BigDecimal findSumByType(OperationType operationType, DepositEntity depositEntity);
}
