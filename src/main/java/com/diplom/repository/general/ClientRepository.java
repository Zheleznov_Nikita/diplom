package com.diplom.repository.general;

import com.diplom.entity.general.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
    @Query("SELECT cl " +
            "FROM ClientEntity cl " +
            "JOIN UserEntity u " +
            "on cl.userEntity.id = u.id " +
            "WHERE u.username = ?1")
    ClientEntity findClientEntityByUsername(String username);
}
