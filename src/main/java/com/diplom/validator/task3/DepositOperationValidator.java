package com.diplom.validator.task3;

import com.diplom.enums.task3.OperationType;
import com.diplom.service.task3.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class DepositOperationValidator {

    @Autowired
    private DepositService depositService;

    public boolean check(Long id, BigDecimal operationSum, String action) {
        return (action.equals(OperationType.FILL.toString()) && operationSum.compareTo(BigDecimal.ZERO) <= 0)
                || (action.equals(OperationType.TAKE.toString())
                && (operationSum.compareTo(BigDecimal.ZERO) <= 0
                || operationSum.compareTo(depositService.findDepositEntityById(id).getBalance()) > 0));
    }
}
