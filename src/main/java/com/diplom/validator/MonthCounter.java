package com.diplom.validator;

import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MonthCounter {
    private final double AVERAGE_MILLIS_PER_MONTH = 365.24 * 24 * 60 * 60 * 1000 / 12;

    public double monthsBetween(Date d1, Date d2) {
        return (d1.getTime() - d2.getTime()) / AVERAGE_MILLIS_PER_MONTH;
    }

    public int monthsBetweenInt(Date d1, Date d2) {
        return (int) ((d1.getTime() - d2.getTime()) / AVERAGE_MILLIS_PER_MONTH);
    }
}
