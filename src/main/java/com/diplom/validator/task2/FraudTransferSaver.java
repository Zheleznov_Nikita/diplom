package com.diplom.validator.task2;

import com.diplom.entity.task2.BankAccountEntity;
import com.diplom.entity.task2.DebitCardEntity;
import com.diplom.entity.task2.transfers.TransferFromAccountEntity;
import com.diplom.entity.task2.transfers.TransferFromCardEntity;
import com.diplom.enums.task2.RequisiteType;
import com.diplom.enums.task2.TransferStatus;
import com.diplom.service.task1.CommissionService;
import com.diplom.service.task2.BankAccountService;
import com.diplom.service.task2.DebitCardService;
import com.diplom.service.task2.transfers.TransferFromAccountService;
import com.diplom.service.task2.transfers.TransferFromCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class FraudTransferSaver {

    private final TransferFromAccountService transferFromAccountService;
    private final TransferFromCardService transferFromCardService;
    private final BankAccountService bankAccountService;
    private final DebitCardService debitCardService;
    private final CommissionService commissionService;

    @Autowired
    public FraudTransferSaver(TransferFromAccountService transferFromAccountService,
                              TransferFromCardService transferFromCardService,
                              BankAccountService bankAccountService,
                              DebitCardService debitCardService,
                              CommissionService commissionService) {
        this.transferFromAccountService = transferFromAccountService;
        this.transferFromCardService = transferFromCardService;
        this.bankAccountService = bankAccountService;
        this.debitCardService = debitCardService;
        this.commissionService = commissionService;
    }

    public void update(Long id, String type, String action) {
        BigDecimal newBalance;
        if (action.equals("submit")) {
            if (type.equals(RequisiteType.BANK_ACCOUNT.toString())) {

                TransferFromAccountEntity transfer = transferFromAccountService.findById(id);
                BankAccountEntity bankAccountEntity = transfer.getSourceEntity();

                BigDecimal subSum = transfer.getSum()
                        .divide(BigDecimal.valueOf(1 - commissionService.findById(1L).getTransferCommission() * 0.01)).setScale(0, RoundingMode.HALF_UP);
                System.out.println(subSum);

                newBalance = bankAccountEntity.getSum().subtract(subSum);
                System.out.println(newBalance);
                bankAccountEntity.setSum(newBalance);
                bankAccountService.save(bankAccountEntity);
                transfer.setStatus(TransferStatus.NORMAL);
                transferFromAccountService.save(transfer);
            } else {
                TransferFromCardEntity transfer = transferFromCardService.findById(id);
                DebitCardEntity debitCardEntity = transfer.getSourceEntity();

                BigDecimal subSum = transfer.getSum()
                        .divide(BigDecimal.valueOf(1 - commissionService.findById(1L).getTransferCommission() * 0.01)).setScale(0, RoundingMode.HALF_UP);
                System.out.println(subSum);

                newBalance = debitCardEntity.getSum().subtract(subSum);
                System.out.println(newBalance);
                debitCardEntity.setSum(newBalance);
                debitCardService.save(debitCardEntity);
                transfer.setStatus(TransferStatus.NORMAL);
                transferFromCardService.save(transfer);
            }
        }
        if (action.equals("decline")) {
            if (type.equals(RequisiteType.BANK_ACCOUNT.toString())) {
                TransferFromAccountEntity transfer = transferFromAccountService.findById(id);
                transfer.setStatus(TransferStatus.BLOCKED);
                transferFromAccountService.save(transfer);
            } else {
                TransferFromCardEntity transfer = transferFromCardService.findById(id);
                transfer.setStatus(TransferStatus.BLOCKED);
                transferFromCardService.save(transfer);
            }
        }
    }
}
