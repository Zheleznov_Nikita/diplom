package com.diplom.validator.task2;

import com.diplom.dto.task2.TransferDto;
import com.diplom.entity.task2.BankAccountEntity;
import com.diplom.entity.task2.DebitCardEntity;
import com.diplom.entity.task2.transfers.*;
import com.diplom.enums.task2.TransferStatus;
import com.diplom.service.task1.CommissionService;
import com.diplom.service.task2.BankAccountService;
import com.diplom.service.task2.DebitCardService;
import com.diplom.service.task2.transfers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;

@Component
public class TransferValidator {
    private final BankAccountService bankAccountService;
    private final DebitCardService debitCardService;
    private final AccountToAccountService accountToAccountService;
    private final AccountToCardService accountToCardService;
    private final CardToAccountService cardToAccountService;
    private final CardToCardService cardToCardService;
    private final TransferFromAccountService transferFromAccountService;
    private final TransferFromCardService transferFromCardService;
    private final CommissionService commissionService;

    @Autowired
    public TransferValidator(BankAccountService bankAccountService,
                             DebitCardService debitCardService,
                             AccountToAccountService accountToAccountService,
                             AccountToCardService accountToCardService,
                             CardToAccountService cardToAccountService,
                             CardToCardService cardToCardService,
                             TransferFromAccountService transferFromAccountService,
                             TransferFromCardService transferFromCardService, CommissionService commissionService) {
        this.bankAccountService = bankAccountService;
        this.debitCardService = debitCardService;
        this.accountToAccountService = accountToAccountService;
        this.accountToCardService = accountToCardService;
        this.cardToAccountService = cardToAccountService;
        this.cardToCardService = cardToCardService;
        this.transferFromAccountService = transferFromAccountService;
        this.transferFromCardService = transferFromCardService;
        this.commissionService = commissionService;
    }

    public void checkInnerTransfer(TransferDto transferDto) {
        if (!transferDto.getSource().equals(transferDto.getRecipient())) {
            DebitCardEntity debitCardEntity;
            BankAccountEntity bankAccountEntity;
            Date date = new Date(Calendar.getInstance().getTime().getTime());
            BigDecimal sum = transferDto.getSum();
            if (transferDto.getSource().length() == 16) {
                debitCardEntity = debitCardService.findByAccountNumber(transferDto.getSource());
                if (checkCardSum(sum, debitCardEntity)) {
                    debitCardEntity.setSum(debitCardEntity.getSum().subtract(sum));
                    debitCardService.save(debitCardEntity);
                    if (transferDto.getRecipient().length() == 16) {
                        CardToCardEntity transferEntity = new CardToCardEntity();
                        transferEntity.setSum(sum);
                        transferEntity.setSourceEntity(debitCardService.findByAccountNumber(transferDto.getSource()));
                        transferEntity.setRecipientEntity(debitCardService.findByAccountNumber(transferDto.getRecipient()));
                        transferEntity.setTransferDate(date);
                        cardToCardService.save(transferEntity);
                        debitCardEntity = debitCardService.findByAccountNumber(transferDto.getRecipient());
                        debitCardEntity.setSum(debitCardEntity.getSum().add(sum));
                        debitCardService.save(debitCardEntity);
                    } else {
                        CardToAccountEntity transferEntity = new CardToAccountEntity();
                        transferEntity.setSum(sum);
                        transferEntity.setSourceEntity(debitCardService.findByAccountNumber(transferDto.getSource()));
                        transferEntity.setRecipientEntity(bankAccountService.findByAccountNumber(transferDto.getRecipient()));
                        transferEntity.setTransferDate(date);
                        cardToAccountService.save(transferEntity);
                        bankAccountEntity = bankAccountService.findByAccountNumber(transferDto.getRecipient());
                        bankAccountEntity.setSum(bankAccountEntity.getSum().add(sum));
                        bankAccountService.save(bankAccountEntity);
                    }
                }
            } else {
                bankAccountEntity = bankAccountService.findByAccountNumber(transferDto.getSource());
                if (checkAccountSum(sum, bankAccountEntity)) {
                    bankAccountEntity.setSum(bankAccountEntity.getSum().subtract(sum));
                    bankAccountService.save(bankAccountEntity);
                    if (transferDto.getRecipient().length() == 16) {
                        AccountToCardEntity transferEntity = new AccountToCardEntity();
                        transferEntity.setSum(sum);
                        transferEntity.setSourceEntity(bankAccountService.findByAccountNumber(transferDto.getSource()));
                        transferEntity.setRecipientEntity(debitCardService.findByAccountNumber(transferDto.getRecipient()));
                        transferEntity.setTransferDate(date);
                        accountToCardService.save(transferEntity);
                        debitCardEntity = debitCardService.findByAccountNumber(transferDto.getRecipient());
                        debitCardEntity.setSum(debitCardEntity.getSum().add(sum));
                        debitCardService.save(debitCardEntity);
                    } else {
                        AccountToAccountEntity transferEntity = new AccountToAccountEntity();
                        transferEntity.setSum(sum);
                        transferEntity.setSourceEntity(bankAccountService.findByAccountNumber(transferDto.getSource()));
                        transferEntity.setRecipientEntity(bankAccountService.findByAccountNumber(transferDto.getRecipient()));
                        transferEntity.setTransferDate(date);
                        accountToAccountService.save(transferEntity);
                        bankAccountEntity = bankAccountService.findByAccountNumber(transferDto.getRecipient());
                        bankAccountEntity.setSum(bankAccountEntity.getSum().add(sum));
                        bankAccountService.save(bankAccountEntity);
                    }
                }
            }
        }
    }

    public void checkTransfer(TransferDto transferDto) {
        if (transferDto.getRecipient().startsWith("111111")) {
            checkInnerTransfer(transferDto);
        } else {
            Date date = new Date(Calendar.getInstance().getTime().getTime());
            BigDecimal sum = transferDto.getSum();
            if (transferDto.getSource().length() == 16) {
                TransferFromCardEntity transferEntity = new TransferFromCardEntity();
                DebitCardEntity debitCardEntity = debitCardService.findByAccountNumber(transferDto.getSource());
                transferEntity.setSourceEntity(debitCardEntity);
                if (checkCardSum(sum, transferEntity.getSourceEntity())) {
                    BigDecimal transferSum = sum.multiply(BigDecimal.valueOf(1 - commissionService.findById(1L).getTransferCommission() * 0.01));
                    transferEntity.setSum(transferSum);
                    transferEntity.setRecipientNumber(transferDto.getRecipient());
                    transferEntity.setTransferDate(date);
                    transferEntity.setStatus(TransferStatus.NORMAL);
                    if (cardFraudCheck(transferEntity, sum)) {
                        debitCardEntity.setSum(debitCardEntity.getSum().subtract(sum));
                        debitCardService.save(debitCardEntity);
                    } else
                        transferEntity.setStatus(TransferStatus.SUSPECT);
                    transferFromCardService.save(transferEntity);
                }
            } else {
                TransferFromAccountEntity transferFromAccountEntity = new TransferFromAccountEntity();
                BankAccountEntity bankAccountEntity = bankAccountService.findByAccountNumber(transferDto.getSource());
                transferFromAccountEntity.setSourceEntity(bankAccountEntity);
                if (checkAccountSum(sum, transferFromAccountEntity.getSourceEntity())) {
                    BigDecimal transferSum = sum.multiply(BigDecimal.valueOf(1 - commissionService.findById(1L).getTransferCommission() * 0.01));
                    transferFromAccountEntity.setSum(transferSum);
                    transferFromAccountEntity.setRecipientNumber(transferDto.getRecipient());
                    transferFromAccountEntity.setTransferDate(date);
                    transferFromAccountEntity.setStatus(TransferStatus.NORMAL);
                    if (accountFraudCheck(transferFromAccountEntity, sum)) {
                        bankAccountEntity.setSum(bankAccountEntity.getSum().subtract(sum));
                        bankAccountService.save(bankAccountEntity);
                    } else
                        transferFromAccountEntity.setStatus(TransferStatus.SUSPECT);
                    transferFromAccountService.save(transferFromAccountEntity);
                }
            }
        }
    }

    public boolean checkAccountSum(BigDecimal sum, BankAccountEntity bankAccountEntity) {
        return bankAccountEntity.getSum().compareTo(sum) >= 0 && sum.compareTo(BigDecimal.ZERO) > 0;
    }

    public boolean checkCardSum(BigDecimal sum, DebitCardEntity debitCardEntity) {
        return debitCardEntity.getSum().compareTo(sum) >= 0 && sum.compareTo(BigDecimal.ZERO) > 0;
    }

    //ограничение количества покупок по одной банковской карте или одним пользователем за определенный период времени
    //ограничение на максимальную сумму разовой покупки по одной карте или одним пользователем в определенный период времени
    public boolean accountFraudCheck(TransferFromAccountEntity transferEntity, BigDecimal sum) {
        if (sum.compareTo(BigDecimal.valueOf(80_000)) < 0) {
            return transferFromAccountService.findCountByAccountNumber(transferEntity.getSourceEntity(), transferEntity.getTransferDate()) < 2;
        } else
            return false;
    }

    public boolean cardFraudCheck(TransferFromCardEntity transferEntity, BigDecimal sum) {
        if (sum.compareTo(BigDecimal.valueOf(80_000)) < 0) {
            return transferFromCardService.findCountByAccountNumber(transferEntity.getSourceEntity(), transferEntity.getTransferDate()) < 2;
        } else
            return false;
    }
}
