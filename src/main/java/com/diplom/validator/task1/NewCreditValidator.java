package com.diplom.validator.task1;

import com.diplom.entity.task1.BidEntity;
import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.entity.task1.CreditEntity;
import com.diplom.service.task1.CreditCardService;
import com.diplom.service.task1.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

@Component
public class NewCreditValidator {
    @Autowired
    private CreditCardService creditCardService;
    @Autowired
    private CreditService creditService;

    public CreditCardEntity getCreditCardEntity(Long id, String username) {
        CreditCardEntity creditCardEntity = creditCardService.findByUsernameAndId(username, id);
        BigDecimal subLimit = creditService.findAllCreditSum(id, username);
        BigDecimal limit = creditCardEntity.getSumLimit();
        if (subLimit != null) {
            limit = limit.subtract(subLimit);
        }
        creditCardEntity.setSumLimit(limit);
        return creditCardEntity;
    }

    public BigDecimal getLimit(Long id, String username) {
        CreditCardEntity creditCardEntity = creditCardService.findByUsernameAndId(username, id);
        BigDecimal subLimit = creditService.findAllCreditSum(id, username);
        BigDecimal limit = creditCardEntity.getSumLimit();
        if (subLimit != null) {
            limit = limit.subtract(subLimit);
        }
        return limit;
    }

    public boolean checkSum(CreditEntity creditEntity, Long id, String username) {
        return creditEntity.getSum().compareTo(getLimit(id, username)) <= 0
                && creditEntity.getSum().compareTo(BigDecimal.ZERO) > 0;
    }


    public boolean checkCreditCardSum(BidEntity bidEntity, String username) {
        return creditService.findCountByExpiredTerm(username) == 0
                && bidEntity.getSumLimit().compareTo(BigDecimal.ZERO) > 0;
    }

    public CreditEntity checkDate(CreditEntity creditEntity) {
        Date startDate = creditEntity.getStartDate();
        Date creditCardEndDate = creditEntity.getCreditCardEntity().getEndDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.DATE, 30);
        Date endDate = calendar.getTime();
        int days = (int) ((creditCardEndDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000));
        if (days < 1) {
            return null;
        } else {
            if (days < 30) {
                calendar.setTime(startDate);
                calendar.add(Calendar.DATE, days);
                endDate = calendar.getTime();
            }
            creditEntity.setEndDate(new java.sql.Date(endDate.getTime()));
            return creditEntity;
        }
    }
}
