package com.diplom.validator.task1;

import com.diplom.entity.general.CommissionEntity;
import com.diplom.entity.task1.CreditCardEntity;
import com.diplom.entity.task1.CreditEntity;
import com.diplom.entity.task1.ValutaEntity;
import com.diplom.enums.task1.CardStatus;
import com.diplom.enums.task1.CreditStatus;
import com.diplom.service.task1.CommissionService;
import com.diplom.service.task1.CreditCardService;
import com.diplom.service.task1.CreditService;
import com.diplom.service.task1.PaymentService;
import com.diplom.validator.MonthCounter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

@Component
public class SumPaymentValidator {
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private CreditService creditService;
    @Autowired
    private CreditCardService creditCardService;
    @Autowired
    private CommissionService commissionService;
    @Autowired
    private MonthCounter monthCounter;

    public boolean checkPaymentSum(BigDecimal sum, Long id, String username) {
        if (sum.compareTo(BigDecimal.ZERO) > 0) {
            if (getResultSum(id, username).compareTo(sum) <= 0) {
                CreditEntity creditEntity = creditService.findCreditEntityByIdAndUsername(id, username);
                creditEntity.setCreditStatus(CreditStatus.REPAID);
                creditService.save(creditEntity);
                if (creditService.findCountByCreditCardIdAndStatus(creditEntity.getCreditCardEntity().getId(), CreditStatus.REPAID) == 0) {
                    CreditCardEntity creditCardEntity = creditEntity.getCreditCardEntity();
                    if (creditCardEntity.getCardStatus().equals(CardStatus.EXPIRED)) {
                        creditCardEntity.setCardStatus(CardStatus.CLOSED);
                        creditCardService.save(creditCardEntity);
                    }
                }
            }
            return true;
        } else
            return false;
    }

    public BigDecimal getResultSum(Long creditId, String username) {
        // Сумма_погашения = (Сумма_кредита + (Сумма_кредита * пени) / 100) - Сумма_всех_платежей_по_этому_кредиту
        // Если Сумма_погашения == 0, то статус_кредита = REPAID
        // Если кредит в другой валюте, то тогда
        // Сумма_погашения = (Сумма_кредита + Сумма_кредита * пени / 100) * RUB_RATE * (1 + комиссия_банку / 100) - Сумма_всех_платежей_по_этому_кредиту
        BigDecimal resultSum;
        BigDecimal sumPaymentsByCreditId = paymentService.findSumPaymentsByCreditId(creditId, username);
        BigDecimal sumCredit = creditService.findCreditSumByIdAndUsername(creditId, username);
        CreditEntity creditEntity = creditService.findCreditEntityByIdAndUsername(creditId, username);
        ValutaEntity valutaEntity = creditService.findValutaByCreditIdAndUsername(creditId, username);
        CommissionEntity commissionEntity = commissionService.findById(1L);
        //todo Доделать подсчет суммы
        resultSum = sumCredit
                .add(
                        (sumCredit.
                                multiply(BigDecimal.valueOf(creditEntity.getPenalization())).
                                multiply(BigDecimal.valueOf(0.01))
                        )
                );
        if (valutaEntity.getRubRate().compareTo(BigDecimal.ONE) != 0) {
            resultSum = resultSum.multiply(valutaEntity.getRubRate()).multiply(
                    (
                            BigDecimal.valueOf(1)
                                    .add(
                                            BigDecimal.valueOf(commissionEntity.getCommission())
                                                    .multiply(BigDecimal.valueOf(0.01)))
                    )
            );
        }
        if (sumPaymentsByCreditId != null)
            resultSum = resultSum.subtract(sumPaymentsByCreditId);
        if (resultSum.compareTo(BigDecimal.ZERO) <= 0)
            resultSum = BigDecimal.ZERO;
        else {
            if (Calendar.getInstance().getTime().compareTo(creditEntity.getEndDate()) > 0) {
                double months = monthCounter.monthsBetween(Calendar.getInstance().getTime(), creditEntity.getEndDate());
                System.out.println(months);
                double percentsCoeff = 1;
                if (months > 1)
                    percentsCoeff = 1 + months * creditEntity.getCreditCardEntity().getPercent() / 12;
                System.out.println(percentsCoeff);
                resultSum = resultSum.multiply(BigDecimal.valueOf(percentsCoeff));
            }
        }
        return resultSum.setScale(1, RoundingMode.UP);
    }
}