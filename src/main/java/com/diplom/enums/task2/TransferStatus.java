package com.diplom.enums.task2;

public enum TransferStatus {
    BLOCKED,
    SUSPECT,
    NORMAL
}
