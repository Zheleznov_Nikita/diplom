package com.diplom.enums.task2;

public enum RequisiteType {
    CARD,
    BANK_ACCOUNT
}
