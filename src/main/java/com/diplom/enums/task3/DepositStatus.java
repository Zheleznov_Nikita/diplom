package com.diplom.enums.task3;

public enum DepositStatus {
    OPEN,
    CLOSED
}
