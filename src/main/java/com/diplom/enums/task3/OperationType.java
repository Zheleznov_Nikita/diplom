package com.diplom.enums.task3;

public enum OperationType {
    FILL,
    TAKE,
    FROM_BANK
}
