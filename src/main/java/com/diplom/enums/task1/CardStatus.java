package com.diplom.enums.task1;

public enum CardStatus {
    IN_PROCESS,
    OPEN,
    CLOSED,
    DECLINED,
    EXPIRED
}
