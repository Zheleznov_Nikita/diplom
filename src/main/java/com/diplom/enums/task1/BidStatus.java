package com.diplom.enums.task1;

public enum BidStatus {
    IN_PROCESS,
    DECLINED,
    APPROVED
}
