package com.diplom.enums.task1;

public enum CreditStatus {
    OPENED,
    EXPIRED,
    REPAID
}
